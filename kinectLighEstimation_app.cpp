/*
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2011, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 *  Author: Sergio Orts , (sorts@dtic.ua.es)
 */

//#include <pcl/gpu/kinfu/kinfu.h>
//#include <pcl/gpu/kinfu/raycaster.h>
//#include <pcl/gpu/kinfu/marching_cubes.h>

#include <iostream>
#include <fstream>
#include <boost/filesystem.hpp>
#include <pcl/console/parse.h>
#include <pcl/common/time.h>
#include <pcl/common/angles.h>
#include <pcl/common/distances.h>
#include <pcl/gpu/containers/initialization.h>
#include <pcl/gpu/containers/device_array.h>
#include <pcl/gpu/containers/device_memory.h>
#include <pcl/gpu/kinfu/pixel_rgb.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/visualization/image_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/point_cloud_handlers.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/png_io.h>
#include <pcl/io/vtk_io.h>
#include <pcl/io/openni_grabber.h>
#include <pcl/io/oni_grabber.h>
#include <pcl/io/pcd_grabber.h>
#include <pcl/exceptions.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/gp3.h>
#include <pcl/surface/organized_fast_mesh.h>
#include <pcl/surface/poisson.h>
#include <pcl/surface/marching_cubes_hoppe.h>
#include <pcl/keypoints/uniform_sampling.h>
#include <pcl/keypoints/sift_keypoint.h>
#include <pcl/keypoints/iss_3d.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/filters/bilateral.h>
#include <pcl/pcl_base.h>
#include <pcl/pcl_macros.h>
#include <pcl/common/pca.h>
#include <pcl/ros/conversions.h>
#include <pcl/segmentation/region_growing_rgb.h>
#include <pcl/segmentation/region_growing.h>

#include <queue>
#include <stack>

#include "include/internal.h"
#include "include/bilateralFilter_CPU.h"
#include "include/segmentation.h"

#define DEBUG_MESSAGES
#undef DEBUG_MESSAGES

typedef pcl::ScopeTime ScopeTimeT;

using namespace std;
using namespace pcl;
using namespace pcl::gpu;
using namespace Eigen;
namespace pc = pcl::console;

namespace pcl
{
  typedef union
  {
    struct
    {
      unsigned char Blue;
      unsigned char Green;
      unsigned char Red;
      unsigned char Alpha;
    };
    float float_value;
    uint32_t long_value;
  } RGBValue;
}

struct Trans
{
  float tx,ty,tz;
  float rx,ry,rz,rw;
};

typedef DeviceArray2D<PixelRGB> View;
typedef DeviceArray2D<unsigned short> DepthMap;
typedef DeviceArray2D<float> MapArr;

typedef pcl::PointXYZ pXYZ;
typedef pcl::Normal nXYZ;

const float sigma_color = 30;     //in mm
const float sigma_space = 4.5;     // in pixels

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

vector<string> getPcdFilesInDir(const string& directory)
{
  namespace fs = boost::filesystem;
  fs::path dir(directory);
  std::cout << "path: " << directory << std::endl;
  if (directory.empty() || !fs::exists(dir) || !fs::is_directory(dir))
    PCL_THROW_EXCEPTION (pcl::IOException, "No valid PCD directory given!\n");
    
  vector<string> result;
  fs::directory_iterator pos(dir);
  fs::directory_iterator end;           

  for(; pos != end ; ++pos)
    if (fs::is_regular_file(pos->status()) )
      if (fs::extension(*pos) == ".pcd")
      {
#if BOOST_FILESYSTEM_VERSION == 3
        result.push_back (pos->path ().string ());
#else
        result.push_back (pos->path ());
#endif
        cout << "added: " << result.back() << endl;
      }
    
  return result;  
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct SampledScopeTime : public StopWatch
{          
  enum { EACH = 33 };
  SampledScopeTime(int& time_ms) : time_ms_(time_ms) {}
  ~SampledScopeTime()
  {
    static int i_ = 0;
    time_ms_ += getTime ();    
    if (i_ % EACH == 0 && i_)
    {
      cout << "Average frame time = " << time_ms_ / EACH << "ms ( " << 1000.f * EACH / time_ms_ << "fps )" << endl;
      time_ms_ = 0;        
    }
    ++i_;
  }
private:    
    int& time_ms_;    
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Eigen::Affine3f 
getViewerPose (visualization::PCLVisualizer& viewer)
{
  Eigen::Affine3f pose = viewer.getViewerPose();
  Eigen::Matrix3f rotation = pose.linear();

  Matrix3f axis_reorder;  
  axis_reorder << 0,  0,  1,
                 -1,  0,  0,
                  0, -1,  0;

  rotation = rotation * axis_reorder;
  pose.linear() = rotation;
  return pose;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename CloudT> void
writeCloudFile (int format, const CloudT& cloud);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void 
writePoligonMeshFile (int format, const pcl::PolygonMesh& mesh);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef pcl::PointXYZRGB PointTC;
pcl::PointCloud<PointTC>::Ptr
convertToXYZRGBPointCloud (const boost::shared_ptr<openni_wrapper::Image> &image,
                                               const boost::shared_ptr<openni_wrapper::DepthImage> &depth_image,
																							 std::string rgb_frame_id,float constant_) // boost::shared_ptr<openni_wrapper::OpenNIDevice> device)
{
	  static unsigned rgb_array_size = 0;
	  static boost::shared_array<unsigned char> rgb_array (0);
	  static unsigned char* rgb_buffer = 0;

		int image_width_=image->getWidth();
		int image_height_=image->getHeight();
		int depth_width_=depth_image->getWidth();
		int depth_height_=depth_image->getHeight();

	  boost::shared_ptr<pcl::PointCloud<PointTC> > cloud (new pcl::PointCloud<PointTC>);

	  cloud->header.frame_id = rgb_frame_id;
	  cloud->height = std::max (image_height_, depth_height_);
	  cloud->width = std::max (image_width_, depth_width_);
	  cloud->is_dense = false;

	  cloud->points.resize (cloud->height * cloud->width);

	  float constant = constant_;//1.0f / -1.f; //device->getImageFocalLength (depth_width_);
	  register int centerX = (depth_width_ >> 1);
	  int centerY = (depth_height_ >> 1);

	  register const XnDepthPixel* depth_map = depth_image->getDepthMetaData ().Data ();
	  if (depth_image->getWidth () != depth_width_ || depth_image->getHeight() != depth_height_)
	  {
		static unsigned buffer_size = 0;
		static boost::shared_array<unsigned short> depth_buffer (0);

		if (buffer_size < depth_width_ * depth_height_)
		{
		  buffer_size = depth_width_ * depth_height_;
		  depth_buffer.reset (new unsigned short [buffer_size]);
		}

		depth_image->fillDepthImageRaw (depth_width_, depth_height_, depth_buffer.get ());
		depth_map = depth_buffer.get ();
	  }

	  // here we need exact the size of the point cloud for a one-one correspondence!
	  if (rgb_array_size < image_width_ * image_height_ * 3)
	  {
		rgb_array_size = image_width_ * image_height_ * 3;
		rgb_array.reset (new unsigned char [rgb_array_size]);
		rgb_buffer = rgb_array.get ();
	  }
	  image->fillRGB (image_width_, image_height_, rgb_buffer, image_width_ * 3);
	  float bad_point = std::numeric_limits<float>::quiet_NaN ();

	  // set xyz to Nan and rgb to 0 (black)  
	  if (image_width_ != depth_width_)
	  {
		PointTC pt;
		pt.x = pt.y = pt.z = bad_point;
		pt.b = pt.g = pt.r = 0;
		pt.a = 255; // point has no color info -> alpha = max => transparent 
		cloud->points.assign (cloud->points.size (), pt);
	  }
  
	  // fill in XYZ values
	  unsigned step = cloud->width / depth_width_;
	  unsigned skip = cloud->width * step - cloud->width;
  
	  int value_idx = 0;
	  int point_idx = 0;
	  for (int v = -centerY; v < centerY; ++v, point_idx += skip)
	  {
			for (register int u = -centerX; u < centerX; ++u, ++value_idx, point_idx += step)
			{
				PointTC& pt = cloud->points[point_idx];
				/// @todo Different values for these cases
				// Check for invalid measurements

				if (depth_map[value_idx] != 0 &&
					depth_map[value_idx] != depth_image->getNoSampleValue () &&
					depth_map[value_idx] != depth_image->getShadowValue ())
				{
					pt.z = depth_map[value_idx] * 0.001f;
					pt.x = static_cast<float> (u) * pt.z * constant;
					pt.y = static_cast<float> (v) * pt.z * constant;
				}
				else
				{
					pt.x = pt.y = pt.z = bad_point;
				}
			}
	  }

	  // fill in the RGB values
	  step = cloud->width / image_width_;
	  skip = cloud->width * step - cloud->width;
  
	  value_idx = 0;
	  point_idx = 0;
	  pcl::RGBValue color;
	  color.Alpha = 0;

	  for (unsigned yIdx = 0; yIdx < image_height_; ++yIdx, point_idx += skip)
	  {
		  for (unsigned xIdx = 0; xIdx < image_width_; ++xIdx, point_idx += step, value_idx += 3)
		  {
		    PointTC& pt = cloud->points[point_idx];
      
		    color.Red   = rgb_buffer[value_idx];
		    color.Green = rgb_buffer[value_idx + 1];
		    color.Blue  = rgb_buffer[value_idx + 2];
      
		    pt.rgba = color.long_value;
		  }
	  }
	  cloud->sensor_origin_.setZero ();
	  cloud->sensor_orientation_.w () = 0.0;
	  cloud->sensor_orientation_.x () = 1.0;
	  cloud->sensor_orientation_.y () = 0.0;
	  cloud->sensor_orientation_.z () = 0.0;

		return (cloud);
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline pcl::PointXYZRGB Eigen2PointXYZRGB(Eigen::Vector3f v, Eigen::Vector3f rgb) { pcl::PointXYZRGB p(rgb[0],rgb[1],rgb[2]); p.x = v[0]; p.y = v[1]; p.z = v[2]; return p; }

void writeArrayToFile(const pcl::PointCloud<float> &arr, std::string filename)
{
	ofstream myfile;
	myfile.open (filename);
	if( myfile.is_open() )
	{
		for(int i=0;i<arr.size();i++ )
		{
			if( pcl_isfinite(arr.points[i]) )
				myfile << arr.points[i] << endl;
		}	
		myfile.close();
	}else
	{
		cerr << "Error can't write array" << endl;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

boost::shared_ptr<pcl::PolygonMesh> convertToMesh(const DeviceArray<pcl::PointXYZ>& triangles)
{ 
  if (triangles.empty())
      return boost::shared_ptr<pcl::PolygonMesh>();

  pcl::PointCloud<pcl::PointXYZ> cloud;
  cloud.width  = (int)triangles.size();
  cloud.height = 1;
  triangles.download(cloud.points);

  boost::shared_ptr<pcl::PolygonMesh> mesh_ptr( new pcl::PolygonMesh() ); 
      
  mesh_ptr->polygons.resize (triangles.size() / 3);
  for (size_t i = 0; i < mesh_ptr->polygons.size (); ++i)
  {
    pcl::Vertices v;
    v.vertices.push_back(i*3+0);
    v.vertices.push_back(i*3+2);
    v.vertices.push_back(i*3+1);              
    mesh_ptr->polygons[i] = v;
  }    
  return mesh_ptr;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct CurrentFrameCloudView
{
  CurrentFrameCloudView() : cloud_device_ (480, 640), cloud_viewer_ ("Frame Cloud Viewer")
  {
    //cloud_ptr_ = PointCloud<PointXYZRGB>::Ptr (new PointCloud<PointXYZRGB>);

    cloud_viewer_.setBackgroundColor (0.94, 0.94, 0.94);
	  //cloud_viewer_.addCoordinateSystem (1.0);
    //cloud_viewer_.initCameraParameters ();
    cloud_viewer_.setSize (640, 480);

		cloud_viewer_.addText ("H: print help", 2, 15, 20, 34, 135, 246); 
  }

  void
  show (const pcl::PointCloud<pXYZ>::Ptr cloud,const pcl::PointCloud<nXYZ>::Ptr normals)//const KinfuTracker& kinfu)
  {
    cloud_viewer_.removeAllPointClouds ();
    cloud_viewer_.addPointCloud(cloud,"scene");
		cloud_viewer_.addPointCloudNormals<pXYZ,nXYZ>(cloud,normals,10,0.05,"normals1"); 
		cloud_viewer_.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 1.0, 0.0, 0.0, "normals1"); 
    cloud_viewer_.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 1, "normals1"); 
  }

	void
  show (const pcl::PointCloud<pXYZ>::Ptr cloud)//const KinfuTracker& kinfu)
  {
    cloud_viewer_.removeAllPointClouds ();
    cloud_viewer_.addPointCloud(cloud,"scene");
  }

  PointCloud<PointXYZRGB>::Ptr cloud_ptr_;
  DeviceArray2D<PointXYZRGB> cloud_device_;
  visualization::PCLVisualizer cloud_viewer_;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct ImageView
{
  ImageView()
  {
    viewerDepth_.setWindowTitle ("Kinect Depth stream");
    viewerDepth_.setPosition (640, 0);
    viewerColor_.setWindowTitle ("Kinect RGB stream");
		viewerColor_.setPosition (0, 480);
		viewerMono_.setWindowTitle ("Grayscale stream");
		viewerMono_.setPosition (1280, 0);
		viewerGeneratedMono_.setWindowTitle ("Generated stream");
		viewerGeneratedMono_.setPosition (1280, 480);
    viewerGeneratedMono_.setWindowTitle ("Generated stream");
    viewerSegmented_.setPosition (640, 480);
    viewerSegmented_.setWindowTitle ("Segmented stream");
  }

  void
  show(const unsigned char *rgb24,const unsigned short *depth, const float *mono, const unsigned char *segmented)
  {
    viewerColor_.addRGBImage (rgb24, 640, 480);
    viewerSegmented_.addRGBImage (segmented, 640, 480);
		viewerDepth_.addShortImage (depth, 640, 480, 0, 5000, true); 
		viewerMono_.addFloatImage(mono,640,480,0.0f,1.0f,true);
		viewerColor_.spinOnce();
		viewerDepth_.spinOnce();
		viewerMono_.spinOnce();
	}
  
  visualization::ImageViewer viewerDepth_;
	visualization::ImageViewer viewerColor_;
	visualization::ImageViewer viewerMono_;
	visualization::ImageViewer viewerGeneratedMono_;
	visualization::ImageViewer viewerGeneratedDepth_;
  visualization::ImageViewer viewerSegmented_;
};

struct KinectApp
{
  enum { PCD_BIN = 1, PCD_ASCII = 2, PLY = 3, MESH_PLY = 7, MESH_VTK = 8 };
  
  KinectApp(pcl::Grabber& source, float vsz,bool gpu,bool offline, std::string path, std::string fileNumber, int maxNumPassesBilateral, bool batch, std::string trajectories_file) : exit_ (false), gpu_(gpu), independent_camera_ (false), integrate_colors_ (true),
		focal_length_(-1.f), capture_ (source), time_ms_(0), offline_(offline), compute_normals_(false), compute_surface_(false), planar_segmentation_(false),rows_(480),cols_(640), record_(false), 
		color_(true),	show_normals_(false), show_segmentation_(false), maxNumPassesBilateral_(maxNumPassesBilateral), batch_(batch), trajectories_file_(trajectories_file)
	{    
		capture_.providesCallback<pcl::ONIGrabber::sig_cb_openni_image_depth_image> ();

		normals_host_ = PointCloud<nXYZ>::Ptr( new PointCloud<nXYZ>() );
		cloud_host_ = PointCloud<pXYZ>::Ptr( new PointCloud<pXYZ>());
		reconstructed_image_data_.resize(rows_*cols_);

		// register callback 3d viewer
		images_view_.viewerDepth_.registerKeyboardCallback (keyboard_callback, (void*)this);

		// initialize normal estimator cpu
		ne.setNormalEstimationMethod (IntegralImageNormalEstimation<pcl::PointXYZRGB, Normal>::COVARIANCE_MATRIX);
		ne.setRectSize(7,7);

		// config organized fast mesh
		ofm.setTrianglePixelSize (2);
		ofm.setTriangulationType (pcl::OrganizedFastMesh<pXYZ>::TRIANGLE_LEFT_CUT);

		// Config extract indices
    extract_.setNegative (true);

		grayscale_conversion_type_ = 1;
		//color_segmentation_threshold_ = 0.005;
		color_segmentation_threshold_ = 0.0065;
    factor_segmentation_threshold_ = 2; //1.75;

		minPointLight_.x = 0.0f; minPointLight_.y = 0.0f; minPointLight_.z = 0.0f;
		frameNumber=0;
		screenshotCounter=0;
		lineNumber=0;
		
		if( offline_ )
		{
			offline_path_ = path;
			offline_filename_ = fileNumber;

      fx_=fy_=0.0019047619;

			if( !gpu )
			{
				distances_pointView_host_.resize(rows_*cols_);
				distances_pointView_host_.width = cols_; distances_pointView_host_.height = rows_;

				directions_pointView_host_.resize(rows_*cols_);
				directions_pointView_host_.width = cols_; directions_pointView_host_.height = rows_;

				distances_pointLight_host_.resize(rows_*cols_);
				distances_pointLight_host_.width = cols_; distances_pointLight_host_.height = rows_;

				directions_pointLight_host_.resize(rows_*cols_);
				directions_pointLight_host_.width = cols_; directions_pointLight_host_.height = rows_;

				attenuation_host_.resize(rows_*cols_);
				reflectance_host_.resize(rows_*cols_);
				albedo_host_.resize(rows_*cols_);
				source_grayscale_image_data_.resize(rows_*cols_);

				mean_distances_host_.resize(rows_*cols_);
				mean_angles_host_.resize(rows_*cols_);
				mean_colors_host_.resize(rows_*cols_);

				mean_distances_host_.width = mean_angles_host_.width = mean_colors_host_.width = cols_;
				mean_distances_host_.height = mean_angles_host_.height = mean_colors_host_.height = rows_;

			}
		}

    showBoth = true;

		// initial grid size in meters
		gridSize_x = 2.5;
		gridSize_y = 2.5;
		gridSize_z = 2.5;
		incr_x = 0.25;
		incr_y = 0.25;
		incr_z = 0.25;
  }

  ~KinectApp()
  {
  }

  void
  readKinFuTransformations ()
  {
    int id;
    float tx,ty,tz,rx,ry,rz,rw;

    std::ifstream input( trajectories_file_ );
    while (input >> id >> tx >> ty >> tz >> rx >> ry >> rz >> rw )
    {
      Trans t;
      t.tx = tx; t.ty = ty; t.tz = tz;
      t.rx = rx; t.ry = ry; t.rz = rz; t.rw = rw;
      trajectories_.push_back (t);
    }
    input.close();
  }

  void
  readCPULightEstimations(const std::string file)
  {
    float x, y, z;
    std::ifstream input(file);
    input >> x;
    input >> y;
    input >> z;

    light_source_pos_cpu_.x = x;
    light_source_pos_cpu_.y = y*-1;
    light_source_pos_cpu_.z = z*-1;

    input.close();
  }

  void
  readGPULightEstimations(const std::string file)
  {
    float x, y, z;
    std::ifstream input(file);
    input >> x;
    input >> y;
    input >> z;

    light_source_pos_gpu_.x = x;
    light_source_pos_gpu_.y = y*-1;
    light_source_pos_gpu_.z = z*-1;

    input.close();
  }

  void
  initCurrentFrameView ()
  {
    current_frame_cloud_view_ = boost::shared_ptr<CurrentFrameCloudView>(new CurrentFrameCloudView ());
    current_frame_cloud_view_->cloud_viewer_.registerKeyboardCallback (keyboard_callback, (void*)this);
  }

	void
  toggleNormals()
  {
    compute_normals_ = !compute_normals_;
    cout << "Compute normals: " << (compute_normals_ ?  "activated" : "deactivated") << endl;
  }

	void
	toggleShowNormals()
	{
		show_normals_ = !show_normals_;
		this->execute (depth_, rgb24_, cloud_host_, normals_host_, true);
    cout << "Show normals: " << (show_normals_ ?  "activated" : "deactivated") << endl;
	}

	void
	toggleShowSegmentation()
	{
		show_segmentation_ = !show_segmentation_;
		this->execute (depth_, rgb24_, cloud_host_, normals_host_, true);
    cout << "Show segmentated cloud: " << (show_segmentation_ ?  "activated" : "deactivated") << endl;
	}

  void
  toggleShowBoth()
  {
    showBoth = !showBoth;
    drawLightAndSensorPositions();
  }

	void
  toggleSurface()
  {
    compute_surface_ = !compute_surface_;
    cout << "Compute surface: " << (compute_surface_ ?  "activated" : "deactivated") << endl;
  }

	void
  toggleRecord()
  {
    record_ = !record_;
    cout << "Recording: " << (record_ ?  "activated" : "deactivated") << endl;
  }

	void
  toggleColor()
  {
    color_ = !color_;
    cout << "Color PointCloud: " << (color_ ?  "activated" : "deactivated") << endl;
  }

	void
  toggleConversionType()
  {
    grayscale_conversion_type_++;
		if( grayscale_conversion_type_ > 2)
			grayscale_conversion_type_=0;
		
		if( grayscale_conversion_type_ == 0 )
			cout << "Grayscale conversion type: AVERAGING "<< endl;
		else if( grayscale_conversion_type_ == 1)
			cout << "Grayscale conversion type: LIGHTING "<< endl;
		else
			cout << "Grayscale conversion type: LUMINOSITY "<< endl;
  }

	void increaseColorSegmentationThreshold()
	{
		color_segmentation_threshold_++;
		cout << "Color segmentation threshold: " << color_segmentation_threshold_ << endl;
	}

	void decreaseColorSegmentationThreshold()
	{
		color_segmentation_threshold_--;
		cout << "Color segmentation threshold: " << color_segmentation_threshold_ << endl;
	}

	void increaseFactorDistanceSegmentationThreshold()
	{
		factor_segmentation_threshold_+=0.1;
		cout << "Factor Cloud Resolution segmentation threshold: " << factor_segmentation_threshold_ << endl;
	}

	void decreaseFactorDistanceSegmentationThreshold()
	{
		factor_segmentation_threshold_-=0.1;
		cout << "Factor Cloud Resolution segmentation threshold: " << factor_segmentation_threshold_ << endl;
	}

	void resetPointLightSource()
	{
		minPointLight_.x = 0.0f;
		minPointLight_.y = 0.0f;
		minPointLight_.z = 0.0f;

		gridSize_x = 3;
		gridSize_y = 3;
		gridSize_z = 3;
		incr_x = 0.10;
		incr_y = 0.10;
		incr_z = 0.10;

		frameNumber = 0;
		cout << "Reset pointLight source to initial Position. " << endl;
	}

	void
  togglePlanarSegmentation()
  {
    if(!compute_normals_) compute_normals_=true;
		planar_segmentation_ = !planar_segmentation_;
		cout << "Planar Segmentation: " << (planar_segmentation_ ?  "activated" : "deactivated") << endl;
  }
  
	void
	computeEnergyGrid(float3 origin, float gridSize_x, float gridSize_y, float gridSize_z, float voxelSize_x, float voxelSize_y, float voxelSize_z)
	{
		float gridSize_x_ = gridSize_x;
		float gridSize_y_ = gridSize_y;
		float gridSize_z_ = gridSize_z;
		float incr_x_ = voxelSize_x;
		float incr_y_ = voxelSize_y;
		float incr_z_ = voxelSize_z;

		std::vector<float> energy_grid;
		int width = (int)(gridSize_x_/voxelSize_x);
		int height = (int)(gridSize_y_/voxelSize_y);
		int length = (int)(gridSize_z_/voxelSize_z);

		energy_grid.resize( width*height*length );

		int steps=0;

		float3 pointView,pointLight;
		pointView.x=pointView.y=pointView.z = 0.0f;

		// tricky attenuation factor obtained for fixed experiments
		float min = std::numeric_limits<float>::max();
		float k=0.0f;
		float energy=0.0f;
		float ix,iy,iz,tx,ty,tz;

		ix = origin.x-gridSize_x_/2;
		iy = origin.y-gridSize_y_/2;
		iz = origin.z-gridSize_z_/2;
		tx = origin.x+gridSize_x_/2;
		ty = origin.y+gridSize_y_/2;
		tz = origin.z+gridSize_z_/2;

		float3 basePointLine;
		basePointLine.x = origin.x;
		basePointLine.y = origin.y;
		basePointLine.z = origin.z;

		for(ix=basePointLine.x-gridSize_x/2;ix<tx;ix+=incr_x_){
			for(iy=basePointLine.y-gridSize_y/2;iy<ty;iy+=incr_y_){
				for(iz=basePointLine.z-gridSize_z/2;iz<tz;iz+=incr_z_)
				{
					float3 voxel_centroid;
					voxel_centroid.x = ix+(incr_x_/2); voxel_centroid.y = iy+(incr_y_/2); voxel_centroid.z = iz+(incr_z_/2);

					//pointLight.x = ix; pointLight.y = iy; pointLight.z = iz;
					energy = computeEnergyGivenPointLight(pointView,voxel_centroid,k,*segments);
					if( energy < min )
					{
						min = energy;
						minPointLight_.x = voxel_centroid.x; minPointLight_.y = voxel_centroid.y; minPointLight_.z = voxel_centroid.z;
					}

					energy_grid[steps]=energy;
					// draw voxel and sphere with energy related size
					drawVoxel(ix,iy,iz,incr_x_,incr_y_,incr_z_);
					steps++;
				}
			}
			//printf("step %i",steps);
		}

		float max = std::numeric_limits<float>::min();
		int imax = std::numeric_limits<int>::min();
		for(int i=0;i<steps;i++)
		{
			if( (1/energy_grid[i]) > max )
			{
				max = (1/energy_grid[i]);
				imax = i;
			}
		}

		steps=0;
		//normalize and draw spheres according to the edge length of the voxel
		for(ix=basePointLine.x-gridSize_x/2;ix<tx;ix+=incr_x_){
			for(iy=basePointLine.y-gridSize_y/2;iy<ty;iy+=incr_y_){
				for(iz=basePointLine.z-gridSize_z/2;iz<tz;iz+=incr_z_)
				{
					pcl::PointXYZ voxel_centroid;
					voxel_centroid.x = ix+(incr_x_/2); voxel_centroid.y = iy+(incr_y_/2); voxel_centroid.z = iz+(incr_z_/2);

					float radius=((1/energy_grid[steps])/max)*(incr_x_/2);
					string saux;
					char aux[100];
					itoa(steps,aux,10);
					saux=aux;
					saux+="sphere";
					current_frame_cloud_view_->cloud_viewer_.addSphere(voxel_centroid,radius,1,0,0,saux);
					steps++;
				}
			}
		}
	}

	void
	drawVoxel(float ix, float iy, float iz, float incr_x, float incr_y, float incr_z)
	{
		PointXYZ p1,p2,p3,p4,p5,p6,p7,p8;
		char aux[100];
		string saux;
		p1.x = ix; p1.y = iy; p1.z = iz;
		p2.x = ix+incr_x; p2.y = iy; p2.z = iz;
		p3.x = ix+incr_x; p3.y = iy; p3.z = iz+incr_z;
		p4.x = ix; p4.y = iy; p4.z = iz+incr_z;
		p5.x = ix; p5.y = iy+incr_y; p5.z = iz;
		p6.x = ix+incr_x; p6.y = iy+incr_y; p6.z = iz;
		p7.x = ix+incr_x; p7.y = iy+incr_y; p7.z = iz + incr_z;
		p8.x = ix; p8.y = iy+incr_y; p8.z = iz+incr_z;
		itoa(++lineNumber,aux,10);
		saux = aux;
		current_frame_cloud_view_->cloud_viewer_.addLine(p1,p2,0,1,0,saux);
		itoa(++lineNumber,aux,10);
		saux = aux;
		current_frame_cloud_view_->cloud_viewer_.addLine(p2,p3,0,1,0,saux);
		itoa(++lineNumber,aux,10);
		saux = aux;
		current_frame_cloud_view_->cloud_viewer_.addLine(p3,p4,0,1,0,saux);
		itoa(++lineNumber,aux,10);
		saux = aux;
		current_frame_cloud_view_->cloud_viewer_.addLine(p4,p1,0,1,0,saux);
		itoa(++lineNumber,aux,10);
		saux = aux;
		current_frame_cloud_view_->cloud_viewer_.addLine(p1,p5,0,1,0,saux);
		itoa(++lineNumber,aux,10);
		saux = aux;
		current_frame_cloud_view_->cloud_viewer_.addLine(p2,p6,0,1,0,saux);
		itoa(++lineNumber,aux,10);
		saux = aux;
		current_frame_cloud_view_->cloud_viewer_.addLine(p3,p7,0,1,0,saux);
		itoa(++lineNumber,aux,10);
		saux = aux;
		current_frame_cloud_view_->cloud_viewer_.addLine(p4,p8,0,1,0,saux);
		itoa(++lineNumber,aux,10);
		saux = aux;
		current_frame_cloud_view_->cloud_viewer_.addLine(p5,p6,0,1,0,saux);
		itoa(++lineNumber,aux,10);
		saux = aux;
		current_frame_cloud_view_->cloud_viewer_.addLine(p6,p7,0,1,0,saux);
		itoa(++lineNumber,aux,10);
		saux = aux;
		current_frame_cloud_view_->cloud_viewer_.addLine(p7,p8,0,1,0,saux);
		itoa(++lineNumber,aux,10);
		saux = aux;
		current_frame_cloud_view_->cloud_viewer_.addLine(p8,p5,0,1,0,saux);
	}

	void
	writeSceneSegmentation()
	{
		std::vector<unsigned char> png_sceneSegmentation;
		png_sceneSegmentation.resize(rows_*cols_*3);
		int counter=0;
		for(int i=0;i<colored_cloud->size();i++)
		{
			png_sceneSegmentation[counter] = colored_cloud->points[i].r;
			png_sceneSegmentation[counter+1] = colored_cloud->points[i].g;
			png_sceneSegmentation[counter+2] = colored_cloud->points[i].b;
			counter += 3;
		}

		char aux[100];
		itoa(frameNumber,aux,10);
		string filename = "segmentation_" + offline_filename_;
		filename += ".png";
		pcl::io::saveRgbPNGFile(filename,&png_sceneSegmentation[0],640,480);
	}

	void
	writeSceneScreenshot()
	{
		char aux[100];
		itoa(screenshotCounter++,aux,10);
		string filename = "screenshot_";
		filename += aux;
		filename += ".png";
		current_frame_cloud_view_->cloud_viewer_.saveScreenshot(filename);
	}

	void
	writeScene()
	{
		char aux[100];
		itoa(frameNumber,aux,10);
		string filename="depth_image_";
		string filename_rgb="rgb_image_";
		string filename_cloud="cloud_";
		string filename_filtered_cloud="triangulated_cloud_";
		filename += aux;
		filename_rgb += aux;
		filename_cloud += aux;
		filename_filtered_cloud += aux;
		filename += ".png";
		filename_rgb += ".png";
		filename_cloud += ".ply";
		filename_filtered_cloud += ".ply";

		//pcl::io::saveShortPNGFile(filename,&data[0],640,480,1);
				
		pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr color_normal_cloud = pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr(new pcl::PointCloud<pcl::PointXYZRGBNormal>());
		color_normal_cloud->points.resize( color_cloud->size() );
		for(int i=0;i<color_cloud->size();i++)
		{
			pcl::PointXYZRGBNormal &pt = color_normal_cloud->points[i];
			pt.x = color_cloud->points[i].x;
			pt.y = color_cloud->points[i].y;
			pt.z = color_cloud->points[i].z;
			pt.r = color_cloud->points[i].r;
			pt.g = color_cloud->points[i].g;
			pt.b = color_cloud->points[i].b;
			pt.normal_x = normals_host_->points[i].normal_x;
			pt.normal_y = normals_host_->points[i].normal_y;
			pt.normal_z = normals_host_->points[i].normal_z;
		}
		//pcl::io::savePLYFileASCII<pcl::PointXYZRGBNormal>(filename_cloud,*color_normal_cloud);
				
		//build a polygonmesh
		pcl::PolygonMesh::Ptr mesh (new pcl::PolygonMesh);
		pcl::toROSMsg<pcl::PointXYZRGBNormal>(*color_normal_cloud,mesh->cloud);
		for(int i=0;i<vertices_->size();i++)
		{
			mesh->polygons.push_back(vertices_->at(i));
		}
		cout << "Saving Mesh PLY File " << filename_filtered_cloud << endl;
		pcl::io::savePLYFile(filename_filtered_cloud,*mesh);
		cout << "Saving Cloud PLY File " << filename_cloud << endl;
		pcl::io::savePLYFileASCII(filename_cloud,*color_normal_cloud);
		cout << "Saving PNG File " << filename_filtered_cloud << endl;
		pcl::io::saveRgbPNGFile(filename_rgb,(unsigned char*)&source_image_data_[0],640,480);

	}

	void readGroundTruthPosition(const string groundTruthFilename)
	{
		string line;
		ifstream myfile (groundTruthFilename);
		int counter=0;
		if (myfile.is_open())
		{
			while ( myfile.good() )
			{
				getline (myfile,line);
				counter++;
				if( counter == 6 ){
					gtMinPointLightFrame0_.x=atof(line.c_str());
				}else if( counter == 7){
					gtMinPointLightFrame0_.y=atof(line.c_str())*-1;
				}else if( counter == 8){
					gtMinPointLightFrame0_.z=atof(line.c_str())*-1;
				}
			}
			myfile.close();
		}
		else cout << "Unable to open file with ground truth positions"; 
	}

	float
	computeEnergyGivenPointLight(float3 pointView, float3 pointLight, float attenuationFactor, std::vector<pcl::PointIndices> &segments)
	{
		// albedo median values
		boost::shared_ptr<std::vector<float>> albedo_medians = boost::shared_ptr<std::vector<float>> (new std::vector<float>( segments.size() ));

		// if the attenuation is no considered is not necessary to calculate distance and direction vector to the point view
		device::computeDistancesMap (vmap_device, pointView, distances_pointView_, directions_pointView_ ,rows_,cols_);
		
		// Computed distances and normalized directions to the given pointLight
		device::computeDistancesMap (vmap_device, pointLight, distances_pointLight_, directions_pointLight_, rows_,cols_);
		device::computeAttenuationReflectanceMaps (distances_pointLight_,distances_pointView_,attenuation_, nmap_device, directions_pointLight_, reflectance_, attenuationFactor, grayscale_device_, albedo_, rows_, cols_);
		
		int c;
		// download albedo and reflectance map
		albedo_.download(albedo_host_.points,c);
		albedo_host_.width = cols_;
		albedo_host_.height = rows_;
		reflectance_.download( reflectance_host_.points,c);
		reflectance_host_.width = cols_;
		reflectance_host_.height = rows_;

		// albedo average of the segments
		//
		for(int i=1;i<segments.size();i++)
		{
			// performances reasons substitute median by meane
			float avg=0.0f;
			int counter=0;
			if( segments[i].indices.size() > 500 )
			{
				for(int j=0;j<segments[i].indices.size();j++)
				{
					float albedo_factor = albedo_host_.points[ segments[i].indices[j] ];
					if( !pcl_isfinite(albedo_factor) ) 
						continue;

					avg+=albedo_factor; 
					counter++;	
				}
				if( counter > 0 )
					albedo_medians->data()[i] = ( avg/counter );
			}
		}

		// Energy 1 = E1 =  sum((1/sum(TF)) .* ((Kd(TF) - Km(TF)).^2)); Img2 = (Km .* Refl);
		//
		float E1=0.0f;
		float E2=0.0f;
		int counter=0;
		for(int i=1;i<segments.size();i++)
		{
			float albedo_mean = albedo_medians->data()[i];
			if( segments[i].indices.size() > 500 )
			{
				for(int j=0;j<segments[i].indices.size();j++)
				{
					int index = segments[i].indices[j];
					float albedo_factor = albedo_host_.points[ index ];
					float reflectance = reflectance_host_[ index ];

					if( !pcl_isfinite(albedo_factor) || !pcl_isfinite(reflectance) ) 
					{
						reconstructed_image_data_[ index ] = 0.0f;
						continue;
					}
					//E1+=(albedo_factor-albedo_mean)*(albedo_factor-albedo_mean);
					reconstructed_image_data_[ index ] = albedo_mean * reflectance;
					E2+=(source_grayscale_image_data_[index] - reconstructed_image_data_[ index ]) * (source_grayscale_image_data_[index] - reconstructed_image_data_[ index ]);
					counter++;	
				}
			}
		}
		if( counter > 0 )
		{
			//E1 /= counter;
			E2 /= counter;
		}
		return E2;
	}

	float
	computeEnergyGivenPointLightCPU(float3 pointView, float3 pointLight, float attenuationFactor, std::vector<pcl::PointIndices> &segments)
	{
		// albedo median values
		boost::shared_ptr<std::vector<float>> albedo_medians = boost::shared_ptr<std::vector<float>> (new std::vector<float>( segments.size() ));

		// if the attenuation is no considered is not necessary to calculate distance and direction vector to the point view
		computeDistancesMapCPU(*offline_cloud_,pointView,distances_pointView_host_,directions_pointView_host_,rows_,cols_);
		//device::computeDistancesMap (vmap_device, pointView, distances_pointView_, directions_pointView_ ,rows_,cols_);
		
		// Computed distances and normalized directions to the given pointLight
		computeDistancesMapCPU(*offline_cloud_,pointLight,distances_pointLight_host_,directions_pointLight_host_,rows_,cols_);
		//device::computeDistancesMap (vmap_device, pointLight, distances_pointLight_, directions_pointLight_, rows_,cols_);
		
		computeAttenuationReflectanceAlbedoMapsCPU(distances_pointLight_host_,distances_pointView_host_,attenuation_host_,*normals_host_,directions_pointLight_host_,reflectance_host_,attenuationFactor,source_grayscale_image_data_,albedo_host_,rows_,cols_);
		//device::computeAttenuationReflectanceMaps (distances_pointLight_,distances_pointView_,attenuation_, nmap_device, directions_pointLight_, reflectance_, attenuationFactor, grayscale_device_, albedo_, rows_, cols_);
		
		int c;
		// download albedo and reflectance map
		/*albedo_.download(albedo_host_.points,c);
		albedo_host_.width = cols_;
		albedo_host_.height = rows_;
		reflectance_.download( reflectance_host_.points,c);
		reflectance_host_.width = cols_;
		reflectance_host_.height = rows_;*/

		// albedo average of the segments
		//
		for(int i=1;i<segments.size();i++)
		{
			// performances reasons substitute median by average
			float avg=0.0f;
			int counter=0;
			if( segments[i].indices.size() > 100 )
			{
				for(int j=0;j<segments[i].indices.size();j++)
				{
					float albedo_factor = albedo_host_.points[ segments[i].indices[j] ];
					if( !pcl_isfinite(albedo_factor) ) 
						continue;

					avg+=albedo_factor; 
					counter++;	
				}
				if( counter > 0 )
					albedo_medians->data()[i] = ( avg/counter );
			}
		}

		// Energy 1 = E1 =  sum((1/sum(TF)) .* ((Kd(TF) - Km(TF)).^2)); Img2 = (Km .* Refl);
		//
		float E1=0.0f;
		float E2=0.0f;
		int counter=0;
		for(int i=1;i<segments.size();i++)
		{
			float albedo_mean = albedo_medians->data()[i];
			if( segments[i].indices.size() > 100 )
			{
				for(int j=0;j<segments[i].indices.size();j++)
				{
					int index = segments[i].indices[j];
					float albedo_factor = albedo_host_.points[ index ];
					float reflectance = reflectance_host_[ index ];

					if( !pcl_isfinite(albedo_factor) || !pcl_isfinite(reflectance) ) 
					{
						reconstructed_image_data_[ index ] = 0.0f;
						continue;
					}
					//E1+=(albedo_factor-albedo_mean)*(albedo_factor-albedo_mean);
					reconstructed_image_data_[ index ] = albedo_mean * reflectance;
					E2+=(source_grayscale_image_data_[index] - reconstructed_image_data_[ index ]) * (source_grayscale_image_data_[index] - reconstructed_image_data_[ index ]);
					counter++;	
				}
			}
		}
		if( counter > 0 )
		{
			//E1 /= counter;
			E2 /= counter;
		}
		return E2;
	}

  void computeErrorToGroundTruthPositions()
  {
    pcl::PointXYZ minPtLight,gtMinPtLight;
    minPtLight.x = minPointLight_.x; minPtLight.y = minPointLight_.y; minPtLight.z = minPointLight_.z;
    gtMinPtLight.x = gtMinPointLight_.x; gtMinPtLight.y = gtMinPointLight_.y; gtMinPtLight.z = gtMinPointLight_.z;

    Eigen::Vector4f pcentroid;
    pcl::compute3DCentroid(*color_cloud,pcentroid);
    pcl::PointXYZ ptcentroid;
    ptcentroid.x = pcentroid(0); ptcentroid.y = pcentroid(1); ptcentroid.z = pcentroid(2); 

    float distance_to_real = pcl::euclideanDistance(gtMinPtLight,minPtLight);
    // calculate mutual angle
    Eigen::Vector3f p = (ptcentroid.getVector3fMap() - minPtLight.getVector3fMap());
    Eigen::Vector3f q = (ptcentroid.getVector3fMap() - gtMinPtLight.getVector3fMap());

    float r = p.dot(q);
    float angle_rad = acos (r/(p.norm()*q.norm())); 
    float angle_deg = (angle_rad * 180) / M_PI; 
    float abs_angle_deg = fabs(angle_deg);
    cout << "frame : " << frameNumber << " : Angle error : " << abs_angle_deg << " : Distance error : " << distance_to_real << " : " << minPointLight_.x << " : " << minPointLight_.y << " : " << minPointLight_.z << endl;

    /*if( batch_ )
    {
      ofstream myfile;
      myfile.open ("C:\\KinectLightEstimation\\Release\\results.txt", ios::in | ios::app);
      if( myfile.is_open() )
      {
              myfile << offline_path_ << " " << offline_filename_;
              myfile << " | Distance: " << distance_to_real; 
              myfile << " | Angle: " << abs_angle_deg; 
              myfile << " | Real: [ " << gtMinPointLight_.x << "," << gtMinPointLight_.y << "," << gtMinPointLight_.z << " ] ";
              myfile << " | Estimated position: [ " << minPointLight_.x << "," << minPointLight_.y << "," << minPointLight_.z << " ] " << endl; 
              myfile.close();
      }else
      {
              cerr << "Error can't write results onto results.txt" << endl;
      }
    }*/

  }

  void drawLightAndSensorPositions()
  {
    pcl::PointXYZ minPtLight,kinectPosition,gtMinPtLight;
    kinectPosition.x = kinectPosition.y = kinectPosition.z = 0.000000001f;
    minPtLight.x = minPointLight_.x; minPtLight.y = minPointLight_.y; minPtLight.z = minPointLight_.z;
		
    // load gpu estimated light source position
    //minPtLight = light_source_pos_gpu_;
    //bool showBoth = false;
    current_frame_cloud_view_->cloud_viewer_.removeAllShapes();

    gtMinPtLight.x = gtMinPointLight_.x; gtMinPtLight.y = gtMinPointLight_.y; gtMinPtLight.z = gtMinPointLight_.z;
    current_frame_cloud_view_->cloud_viewer_.addSphere(minPtLight,0.05,1,0,1,"light");
    current_frame_cloud_view_->cloud_viewer_.addSphere(gtMinPtLight,0.05,0,0,1,"gtlight");
    if (showBoth) current_frame_cloud_view_->cloud_viewer_.addSphere(light_source_pos_cpu_, 0.05, 1, 0, 0, "cpu_estimated");
    current_frame_cloud_view_->cloud_viewer_.addSphere(kinectPosition,0.05,0,1,0,"kinect");

    current_frame_cloud_view_->cloud_viewer_.addText3D("Sensor position",kinectPosition,0.08,0,1,0,"sensor");
    current_frame_cloud_view_->cloud_viewer_.addText3D("GPU Estimated Point Light",minPtLight,0.08,1,0,1,"estimated");
    if (showBoth) current_frame_cloud_view_->cloud_viewer_.addText3D("CPU Estimated Point Light", light_source_pos_cpu_, 0.08, 1, 0, 0, "cpu_estimated2");
    current_frame_cloud_view_->cloud_viewer_.addText3D("Real Point Light position",gtMinPtLight,0.08,0,0,1,"real");

    Eigen::Vector4f pcentroid;
    pcl::compute3DCentroid(*color_cloud,pcentroid);
    pcl::PointXYZ ptcentroid;
    ptcentroid.x = pcentroid(0); ptcentroid.y = pcentroid(1); ptcentroid.z = pcentroid(2); 

    
    current_frame_cloud_view_->cloud_viewer_.addArrow<pcl::PointXYZ, pcl::PointXYZ>(ptcentroid, minPtLight, 1, 0, 1, false, "arrow1");
    current_frame_cloud_view_->cloud_viewer_.addLine<pcl::PointXYZ, pcl::PointXYZ>(ptcentroid, minPtLight, 1, 0, 1, "distance1");
    current_frame_cloud_view_->cloud_viewer_.setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_LINE_WIDTH,5,"distance1");
    current_frame_cloud_view_->cloud_viewer_.addArrow<pcl::PointXYZ,pcl::PointXYZ>(ptcentroid,gtMinPtLight,0,0,1,false,"arrow2");
    current_frame_cloud_view_->cloud_viewer_.addLine<pcl::PointXYZ,pcl::PointXYZ>(ptcentroid,gtMinPtLight,0,0,1,"distance2");
    current_frame_cloud_view_->cloud_viewer_.setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_LINE_WIDTH,5,"distance2");
    if (showBoth)
    {
      current_frame_cloud_view_->cloud_viewer_.addArrow<pcl::PointXYZ, pcl::PointXYZ>(ptcentroid, light_source_pos_cpu_, 1, 0, 0, false, "arrow3");
      current_frame_cloud_view_->cloud_viewer_.addLine<pcl::PointXYZ, pcl::PointXYZ>(ptcentroid, light_source_pos_cpu_, 1, 0, 0, "distance3");
    }
    current_frame_cloud_view_->cloud_viewer_.setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 5, "distance3");
  }

  void execute(const PtrStepSz<const unsigned short>& depth, const PtrStepSz<const PixelRGB>& rgb24,
							const PointCloud<pXYZ>::Ptr cloud, const PointCloud<nXYZ>::Ptr normals, bool has_data)
  {        
    bool has_image = false;
    char name[1024];

		pcl::PointCloud<pXYZ>::ConstPtr temp_cloud;
		boost::shared_ptr<std::vector<pcl::Vertices> > temp_verts;

		color_cloud = pcl::PointCloud<pcl::PointXYZRGBA>::Ptr(new  pcl::PointCloud<pcl::PointXYZRGBA>());

    if (has_data)
    {
			// if GPU computation enabled
			if(gpu_)
			{		
				#ifdef DEBUG_MESSAGES
					printf("=======================================================================================: \n");
				#endif

				//pcl::ScopeTime timer("Total computing time GPU:");

				// Fill RGB Image and depth map
				if(offline_)
				{
					depth_.cols = cols_;
					depth_.rows = rows_;
					//unsigned short 2 bytes
					depth_.step = depth_.cols * 2;
					source_depth_data_.resize(depth_.cols * depth_.rows);
					//depth_wrapper->fillDepthImageRaw(depth_wrapper->getWidth(), depth_wrapper->getHeight(), &source_depth_data_[0]);
					for(int i=0;i<offline_cloud_->size();i++){
						if( offline_cloud_->points[i].z != 0 ) 
							source_depth_data_[i] = (unsigned short)(offline_cloud_->points[i].z*1000.f);
					}
					depth_.data = &source_depth_data_[0];      
      
					rgb24_.cols = cols_;
					rgb24_.rows = rows_;
					rgb24_.step = rgb24_.cols * 3; 
					source_image_data_.resize(rgb24_.cols * rgb24_.rows);
					//image_wrapper->fillRGB(image_wrapper->getWidth(), image_wrapper->getHeight(), (unsigned char*)&source_image_data_[0]);
					for(int i=0;i<offline_cloud_->size();i++)
					{
						PixelRGB colorPixel; colorPixel.r = offline_cloud_->points[i].r; colorPixel.g = offline_cloud_->points[i].g; colorPixel.b = offline_cloud_->points[i].b;
						source_image_data_[i] = colorPixel;
					}
					rgb24_.data = &source_image_data_[0]; 
				}

				#ifdef DEBUG_MESSAGES
				{
					pcl::ScopeTime timer("Depth map data transfer to the GPU:");
				#endif	
					depth_device_.upload (depth.data, depth.step, depth.rows, depth.cols);
				#ifdef DEBUG_MESSAGES
					pcl::device::sync();
				}
				#endif
				
				if (integrate_colors_)
				{
					// upload image rgb to the GPU
					rgb_device_.upload(rgb24.data, rgb24.step, rgb24.rows, rgb24.cols);

					// HSV and GrayScale conversion
					int ccc;

					//device::bilateralColorFilter(rgb_device_,rgb_bilateral_device_);
					device::convertRGBtoHSV(depth.rows,depth.cols,rgb_device_,hsv_device_);
					device::convertRGBtoGray(depth.rows,depth.cols,rgb_device_,grayscale_device_,grayscale_conversion_type_);
					source_hsv_image_data_.resize(depth.rows*depth.cols);
					source_grayscale_image_data_.resize(depth.rows*depth.cols);
					hsv_device_.download(source_hsv_image_data_,ccc);
					grayscale_device_.download(source_grayscale_image_data_,ccc);
					rgb_device_.download(source_image_data_,ccc);
				}

				// create vertex and normals map
				float centerX = depth.cols >> 1;
				float centerY = depth.rows >> 1;
				
				#ifdef DEBUG_MESSAGES
				{
					pcl::ScopeTime timer("Bilateral filter:");
				#endif
					int numPassesBilateral=0;
					if( maxNumPassesBilateral_>1 )
					{
						do{
							if( numPassesBilateral%2 == 0 )
								device::bilateralFilter (depth_device_, depth_device_bilateral_);
							else
								device::bilateralFilter (depth_device_bilateral_, depth_device_);
							numPassesBilateral++;
						}while(numPassesBilateral<maxNumPassesBilateral_);
					}else
					{
						if(!offline_)
							device::bilateralFilter (depth_device_, depth_device_bilateral_);
						else
							depth_device_bilateral_ = depth_device_;
					}
				#ifdef DEBUG_MESSAGES
					pcl::device::sync ();
				}
				#endif
				
				#ifdef DEBUG_MESSAGES
				{
					pcl::ScopeTime timer("PointCloud:");
				#endif
					device::createVMap (fx_,fy_,depth_device_bilateral_,vmap_device);
				#ifdef DEBUG_MESSAGES
						pcl::device::sync ();
				}
				#endif	

				#ifdef DEBUG_MESSAGES
				{
					pcl::ScopeTime timer("Normals Estimation GPU took:");
				#endif
					device::computeNormalsEigen (vmap_device, nmap_device);
				#ifdef DEBUG_MESSAGES
					pcl::device::sync ();
				}
				#endif

				#ifdef DEBUG_MESSAGES
				// graph creation
				{
					pcl::ScopeTime timer("Compute Distances Graph:");
				#endif
					//device::computeSegmentationGraph(vmap_device,rgb_device_,nmap_device,distances_graph_device_,color_d_graph_device_,normal_angles_graph_device_,depth.rows,depth.cols);
					device::computeSegmentationGraphHSV(vmap_device,hsv_device_,nmap_device,distances_graph_device_,color_d_graph_device_,normal_angles_graph_device_,mean_distances_,mean_colors_,mean_angles_,depth.rows,depth.cols);
				#ifdef DEBUG_MESSAGES
					pcl::device::sync ();
				}
				#endif

				#ifdef DEBUG_MESSAGES
				{
					pcl::ScopeTime timer("Transfer Compute Distances Graph:");
				#endif
					int steps=0;
					distances_graph_host_ = boost::shared_ptr<pcl::PointCloud<pcl::device::float8>>(new pcl::PointCloud<pcl::device::float8>());
					color_d_graph_host_ = boost::shared_ptr<pcl::PointCloud<pcl::device::float8>>(new pcl::PointCloud<pcl::device::float8>());
					normal_angles_graph_host_ = boost::shared_ptr<pcl::PointCloud<pcl::device::float8>>(new pcl::PointCloud<pcl::device::float8>());

					distances_graph_device_.download(distances_graph_host_->points,steps);
					color_d_graph_device_.download(color_d_graph_host_->points,steps);
					normal_angles_graph_device_.download(normal_angles_graph_host_->points,steps);

				#ifdef DEBUG_MESSAGES
					pcl::device::sync ();
				}
				#endif

				float cloud_resolution=0.0f;
				#ifdef DEBUG_MESSAGES
				{
					pcl::ScopeTime timer("Compute Cloud Resolution:");
				#endif
					cloud_resolution=device::computeCloudResolution(vmap_device,depth.rows,depth.cols);
				#ifdef DEBUG_MESSAGES	
					pcl::device::sync ();
				}
				#endif

				#ifdef DEBUG_MESSAGES	
				{
					pcl::ScopeTime timer("PointCloud conversion GPU took:");
				#endif
					organized_points_device_.create (rows_, cols_);
					DeviceArray2D<float4>& c = (DeviceArray2D<float4>&)organized_points_device_;
					device::convert (vmap_device, c);
				#ifdef DEBUG_MESSAGES	
					pcl::device::sync ();
				}
				#endif

				#ifdef DEBUG_MESSAGES	
				{
					pcl::ScopeTime timer("Normals conversion GPU took:");
				#endif
					organized_normals_device_.create (rows_, cols_);
					DeviceArray2D<device::float8>& n = (DeviceArray2D<device::float8>&)organized_normals_device_;
					device::convert (nmap_device, n);
				#ifdef DEBUG_MESSAGES	
					pcl::device::sync ();
				}
				#endif
	
				int cc;
				// points extraction
				#ifdef DEBUG_MESSAGES	
				{
					pcl::ScopeTime timer("PointCloud transfer GPU took:");
				#endif
					organized_points_device_.download(cloud_host_->points,cc);
					cloud_host_->width = organized_points_device_.cols();
					cloud_host_->height = organized_points_device_.rows();
					cloud_host_->is_dense = false;
					//cloud_host_->sensor_origin_.setZero ();
					//cloud_host_->sensor_orientation_.w () = 0.0;
					//cloud_host_->sensor_orientation_.x () = 1.0;
					//cloud_host_->sensor_orientation_.y () = 0.0;
					//cloud_host_->sensor_orientation_.z () = 0.0;
				#ifdef DEBUG_MESSAGES	
				}
				#endif

        // normals extraction
				#ifdef DEBUG_MESSAGES	
				{
					pcl::ScopeTime timer("Normals transfer GPU took:");
				#endif
					organized_normals_device_.download(normals_host_->points,cc);
					normals_host_->width = organized_normals_device_.cols();
					normals_host_->height = organized_normals_device_.rows();
					normals_host_->is_dense = false;
				#ifdef DEBUG_MESSAGES	
				}
				#endif

				// adding cloud color information
				if( color_ )
				{
					pcl::RGBValue color;
					color.Alpha = 0;

					color_cloud->resize(cloud_host_->width * cloud_host_->height );
					color_cloud->width = cloud_host_->width;
					color_cloud->height = cloud_host_-> height;
					for(int i=0;i<cloud_host_->size();i++)
					{
						pcl::PointXYZRGBA &pt = color_cloud->points[i];
						pt.x = cloud_host_->points[i].x; pt.y = cloud_host_->points[i].y; pt.z = cloud_host_->points[i].z; 
						color.Red = source_image_data_[i].r;//rgb_buffer.get()[i*3];
						color.Green = source_image_data_[i].g;//rgb_buffer.get()[i*3+1];
						color.Blue = source_image_data_[i].b;//rgb_buffer.get()[i*3+2];
						pt.rgba = color.long_value;
					}
					color_cloud->is_dense = false;
					//color_cloud->sensor_origin_.setZero ();
					//color_cloud->sensor_orientation_.w () = 0.0;
					//color_cloud->sensor_orientation_.x () = 1.0;
					//color_cloud->sensor_orientation_.y () = 0.0;
					//color_cloud->sensor_orientation_.z () = 0.0;
				}

				#ifdef DEBUG_MESSAGES
				{
					pcl::ScopeTime timer("Growing Region Segmentation CPU took: ");
				#endif
					segments = growingRegionSegmentation(*distances_graph_host_,*color_d_graph_host_,*normal_angles_graph_host_,*color_cloud,cloud_resolution*factor_segmentation_threshold_,color_segmentation_threshold_);
				#ifdef DEBUG_MESSAGES
				}
				#endif

				#ifdef DEBUG_MESSAGES
				{
					pcl::ScopeTime timer("Colouring cloud CPU took: ");
				#endif
					colored_cloud = getColoredCloud(segments,*color_cloud);
				#ifdef DEBUG_MESSAGES
				}
				#endif

        if( frameNumber > 0 )
        {
          Eigen::Matrix<float,3,1> gttrans1( trajectories_[0].tx, trajectories_[0].ty, trajectories_[0].tz );
          Eigen::Quaternion<float> gtrot1 ( trajectories_[0].rx, trajectories_[0].ry, trajectories_[0].rz, trajectories_[0].rw );
          Eigen::Transform<float, 3, Eigen::Affine> tgt1 (Eigen::Translation<float, 3>(gttrans1) * gtrot1);
          
          Eigen::Matrix<float,3,1> gttrans2( trajectories_[frameNumber].tx, trajectories_[frameNumber].ty, trajectories_[frameNumber].tz );
          Eigen::Quaternion<float> gtrot2 ( trajectories_[frameNumber].rx, trajectories_[frameNumber].ry, trajectories_[frameNumber].rz, trajectories_[frameNumber].rw );
          Eigen::Transform<float, 3, Eigen::Affine> tgt2 (Eigen::Translation<float, 3>(gttrans2) * gtrot2);

          pcl::PointXYZ paux( gtMinPointLightFrame0_.x, gtMinPointLightFrame0_.y, gtMinPointLightFrame0_.z );
          Eigen::Transform<float, 3, Eigen::Affine> tgtdif(tgt2.inverse()*tgt1);

          pcl::PointXYZ new_light_pos = pcl::transformPoint(paux, tgtdif.inverse());

          gtMinPointLight_.x = new_light_pos.x; gtMinPointLight_.y = new_light_pos.y; gtMinPointLight_.z = new_light_pos.z;  

          //printf(" %i : %f : %f : %f \n",frameNumber, new_light_pos.x, new_light_pos.y, new_light_pos.z);  
          
          //transformed_cloud = pcl::PointCloud<pcl::PointXYZRGBA>::Ptr(new  pcl::PointCloud<pcl::PointXYZRGBA>());
          //transformed_cloud->resize(cloud_host_->width * cloud_host_->height );
          //pcl::transformPointCloud(*color_cloud, *transformed_cloud, tgtdif);
        }else
        {
          gtMinPointLight_.x = gtMinPointLightFrame0_.x; gtMinPointLight_.y = gtMinPointLightFrame0_.y; gtMinPointLight_.z = gtMinPointLightFrame0_.z;  
          //printf(" %i : %f : %f : %f \n",frameNumber, gtMinPointLight_.x, gtMinPointLight_.y, gtMinPointLight_.z);  
        }

				float3 pointView,pointLight;
				pointView.x=pointView.y=pointView.z = 0.0f;

        // reset pointlight
        //minPointLight_.x = 0.0f; minPointLight_.y = 0.0f; minPointLight_.z = 0.0f;

				// tricky attenuation factor obtained for fixed experiments
				float min = std::numeric_limits<float>::max();
        float k = 0.212f; //1.0f;//0.212f;
				float energy=0.0f;
				float ix,iy,iz,tx,ty,tz;

				//if( frameNumber > 0 && frameNumber < 2 )
				//{
				//	gridSize_x = gridSize_x/2;
				//	gridSize_y = gridSize_y/2;
				//	gridSize_z = gridSize_z/2;
				//}

        gridSize_x = 2.5;
		    gridSize_y = 2.5;
		    gridSize_z = 2.5;
		    incr_x = 0.25;
		    incr_y = 0.25;
		    incr_z = 0.25;

        //for(int it=0;it<4;it++)
        {
				  ix = minPointLight_.x-gridSize_x/2;
				  iy = minPointLight_.y-gridSize_y/2;
				  iz = minPointLight_.z-gridSize_z/2;
				  tx = minPointLight_.x+(gridSize_x/2);
				  ty = minPointLight_.y+(gridSize_y/2);
				  tz = minPointLight_.z+(gridSize_z/2);

				  float3 basePointLine;
				  basePointLine.x = minPointLight_.x;
				  basePointLine.y = minPointLight_.y;
				  basePointLine.z = minPointLight_.z;

				  {
					  #ifdef DEBUG_MESSAGES
						  pcl::ScopeTime timer("Compute Energy Given PointLight: ");
					  #endif
					  for(ix=basePointLine.x-(gridSize_x/2);ix<tx;ix+=incr_x)
						  for(iy=basePointLine.y-(gridSize_y/2);iy<ty;iy+=incr_y)
							  for(iz=basePointLine.z-(gridSize_z/2);iz<tz;iz+=incr_z)
							  {
								  pointLight.x = ix; pointLight.y = iy; pointLight.z = iz;
								  energy = computeEnergyGivenPointLight(pointView,pointLight,k,*segments);
								  if( energy < min )
								  {
									  min = energy;
									  minPointLight_.x = ix; minPointLight_.y = iy; minPointLight_.z = iz;
								  }
							  }
				  }
          
          gridSize_x = gridSize_x/2;
					gridSize_y = gridSize_y/2;
					gridSize_z = gridSize_z/2;
          incr_x = incr_x/2;
		      incr_y = incr_y/2;
		      incr_z = incr_z/2;
        }

        std::cout << minPointLight_.x << " " << minPointLight_.y << " " << minPointLight_.z << std::endl;

				current_frame_cloud_view_->cloud_viewer_.removeAllShapes();

				/*float3 origin; origin.x = origin.y = origin.z = 0;
				computeEnergyGrid(origin,4,4,4,0.5,0.5,0.5);*/
				
				#ifdef DEBUG_MESSAGES
				{
					pcl::ScopeTime timer("Compute Energy Given PointLight: ");
				#endif
					energy = computeEnergyGivenPointLight(pointView,minPointLight_,k,*segments);
				#ifdef DEBUG_MESSAGES
				}
				#endif
          
        drawLightAndSensorPositions();

        computeErrorToGroundTruthPositions();

      }else /////// CPU VERSION /////////
			{
				pcl::ScopeTime timer("Total computing time CPU:");

				// Fill RGB Image and depth map
				if(offline_)
				{
					depth_.cols = cols_;
					depth_.rows = rows_;
					//unsigned short 2 bytes
					depth_.step = depth_.cols * 2;
					source_depth_data_.resize(depth_.cols * depth_.rows);
					//depth_wrapper->fillDepthImageRaw(depth_wrapper->getWidth(), depth_wrapper->getHeight(), &source_depth_data_[0]);
					for(int i=0;i<offline_cloud_->size();i++){
						if( offline_cloud_->points[i].z != 0 && offline_cloud_->points[i].z != std::numeric_limits<float>::quiet_NaN() ) 
							source_depth_data_[i] = (unsigned short)(offline_cloud_->points[i].z*1000.f);
					}
					depth_.data = &source_depth_data_[0];      
      
					rgb24_.cols = cols_;
					rgb24_.rows = rows_;
					rgb24_.step = rgb24_.cols * 3; 
					source_image_data_.resize(rgb24_.cols * rgb24_.rows);
					//image_wrapper->fillRGB(image_wrapper->getWidth(), image_wrapper->getHeight(), (unsigned char*)&source_image_data_[0]);
					for(int i=0;i<offline_cloud_->size();i++)
					{
						PixelRGB colorPixel; colorPixel.r = offline_cloud_->points[i].r; colorPixel.g = offline_cloud_->points[i].g; colorPixel.b = offline_cloud_->points[i].b;
						source_image_data_[i] = colorPixel;
					}
					rgb24_.data = &source_image_data_[0]; 
				}

				filtered_depth_data_.resize(rows_*cols_);
				
				#ifdef DEBUG_MESSAGES
				{
					pcl::ScopeTime time("Time for computing bilateral filtering onto CPU");
				#endif
					bilateralFilter_CPU(source_depth_data_,filtered_depth_data_,0.5f / (sigma_space * sigma_space), 0.5f / (sigma_color * sigma_color),cols_,rows_);
				#ifdef DEBUG_MESSAGES
				}
				#endif

				for(int i=0;i<offline_cloud_->size();i++){
					if( offline_cloud_->points[i].z != 0 ) 
						offline_cloud_->points[i].z = filtered_depth_data_[i]/1000.f;
				}

				#ifdef DEBUG_MESSAGES
				{
					pcl::ScopeTime time("Time for computing normals onto CPU");
				#endif
					ne.setInputCloud (offline_cloud_);
					ne.compute (*normals_host_);
				#ifdef DEBUG_MESSAGES
				}
				#endif

				distances_graph_host_ = boost::shared_ptr<pcl::PointCloud<pcl::device::float8>>(new pcl::PointCloud<pcl::device::float8>());
				distances_graph_host_->resize(rows_*cols_);
				color_d_graph_host_ = boost::shared_ptr<pcl::PointCloud<pcl::device::float8>>(new pcl::PointCloud<pcl::device::float8>());
				color_d_graph_host_->resize(rows_*cols_);
				normal_angles_graph_host_ = boost::shared_ptr<pcl::PointCloud<pcl::device::float8>>(new pcl::PointCloud<pcl::device::float8>());
				
				#ifdef DEBUG_MESSAGES
				{
					pcl::ScopeTime timer("compute Segmentation graphs CPU took: ");
				#endif
					computeSegmentationGraphCPU(*offline_cloud_,*distances_graph_host_,*color_d_graph_host_,*normal_angles_graph_host_);
				#ifdef DEBUG_MESSAGES
				}
				#endif

				// adding cloud color information
				if( color_ )
				{
					pcl::RGBValue color;
					color.Alpha = 0;

					color_cloud->resize(offline_cloud_->width * offline_cloud_->height );
					color_cloud->width = offline_cloud_->width;
					color_cloud->height = offline_cloud_-> height;
					for(int i=0;i<offline_cloud_->size();i++)
					{
						pcl::PointXYZRGBA &pt = color_cloud->points[i];
						pt.x = offline_cloud_->points[i].x; pt.y = offline_cloud_->points[i].y; pt.z = offline_cloud_->points[i].z; 
						color.Red = source_image_data_[i].r;//rgb_buffer.get()[i*3];
						color.Green = source_image_data_[i].g;//rgb_buffer.get()[i*3+1];
						color.Blue = source_image_data_[i].b;//rgb_buffer.get()[i*3+2];
						pt.rgba = color.long_value;
					}
					color_cloud->is_dense = false;
					color_cloud->sensor_origin_.setZero ();
					color_cloud->sensor_orientation_.w () = 0.0;
					color_cloud->sensor_orientation_.x () = 1.0;
					color_cloud->sensor_orientation_.y () = 0.0;
					color_cloud->sensor_orientation_.z () = 0.0;
				}

        //pcl::PointCloud <pcl::PointXYZRGBA>::Ptr segmented_cloud = reg.getColoredCloudRGBA();

				#ifdef DEBUG_MESSAGES
				{
					pcl::ScopeTime timer("GrayScale conversion CPU took: ");
				#endif
					convertRGBtoGrayKernel (rows_,cols_,*offline_cloud_,source_grayscale_image_data_,grayscale_conversion_type_);
				#ifdef DEBUG_MESSAGES
				}
				#endif

				float cloud_resolution = 0.0;
				#ifdef DEBUG_MESSAGES
				{
					pcl::ScopeTime timer("compute Cloud Resolution CPU took: ");
				#endif
					cloud_resolution = computeCloudResolution(offline_cloud_);
				#ifdef DEBUG_MESSAGES
				}
				#endif

				cout << "Cloud resolution : " << cloud_resolution << endl;

				#ifdef DEBUG_MESSAGES
				{
					pcl::ScopeTime timer("Growing Region Segmentation CPU took: ");
				#endif
					segments = growingRegionSegmentation(*distances_graph_host_,*color_d_graph_host_,*normal_angles_graph_host_,*color_cloud,cloud_resolution*factor_segmentation_threshold_,color_segmentation_threshold_);
				#ifdef DEBUG_MESSAGES
				}
				#endif

        colored_cloud = getColoredCloud(segments,*color_cloud);

				float3 pointView,pointLight;
				pointView.x=pointView.y=pointView.z = 0.0f;

				// tricky attenuation factor obtained for fixed experiments
				float min = std::numeric_limits<float>::max();
				float k=1.0f;//0.212f;
				float energy=0.0f;
				float ix,iy,iz,tx,ty,tz;

				if( frameNumber > 0 && frameNumber < 4 )
				{
					gridSize_x = gridSize_x/2;
					gridSize_y = gridSize_y/2;
					gridSize_z = gridSize_z/2;
					incr_x = incr_x/2;
					incr_y = incr_y/2;
					incr_z = incr_z/2;
				}

				ix = minPointLight_.x-gridSize_x/2;
				iy = minPointLight_.y-gridSize_y/2;
				iz = minPointLight_.z-gridSize_z/2;
				tx = minPointLight_.x+gridSize_x/2;
				ty = minPointLight_.y+gridSize_y/2;
				tz = minPointLight_.z+gridSize_z/2;

				float3 basePointLine;
				basePointLine.x = minPointLight_.x;
				basePointLine.y = minPointLight_.y;
				basePointLine.z = minPointLight_.z;

				int steps=0;
				{
					#ifdef DEBUG_MESSAGES
						pcl::ScopeTime timer("Compute Energy Given PointLight: ");
					#endif
					for(ix=basePointLine.x-gridSize_x/2;ix<tx;ix+=incr_x)
						for(iy=basePointLine.y-gridSize_y/2;iy<ty;iy+=incr_y)
							for(iz=basePointLine.z-gridSize_z/2;iz<tz;iz+=incr_z)
							{
								pointLight.x = ix; pointLight.y = iy; pointLight.z = iz;
								energy = computeEnergyGivenPointLightCPU(pointView,pointLight,k,*segments);
								if( energy < min )
								{
									min = energy;
									minPointLight_.x = ix; minPointLight_.y = iy; minPointLight_.z = iz;
								}
								steps++;
							}
				}
				//cout << "Number of steps: " << steps <<endl;*/
				current_frame_cloud_view_->cloud_viewer_.removeAllShapes();

				float3 origin; origin.x = origin.y = origin.z = 0;
				//computeEnergyGrid(origin,5.0,5.0,5.0,0.5,0.5,0.5);
				
				#ifdef DEBUG_MESSAGES
				{
					pcl::ScopeTime timer("Compute Energy Given PointLight: ");
				#endif		
					energy = computeEnergyGivenPointLightCPU(pointView,minPointLight_,k,*segments);
				#ifdef DEBUG_MESSAGES
				}
				#endif

			}
			
      // prepare segmented stream for being showed
      std::vector<unsigned char> segmented_stream;
		  segmented_stream.resize(rows_*cols_*3);
		  int counter=0;
		  for(int i=0;i<colored_cloud->size();i++)
		  {
			  segmented_stream[counter] = colored_cloud->points[i].r;
			  segmented_stream[counter+1] = colored_cloud->points[i].g;
			  segmented_stream[counter+2] = colored_cloud->points[i].b;
			  counter += 3;
		  }

			if( !batch_ )
				images_view_.show((unsigned char*)&source_image_data_[0],(unsigned short*)&source_depth_data_[0],(float*)&source_grayscale_image_data_[0],(unsigned char*)&segmented_stream[0]);

			if( !batch_ )
			{
				if(!gpu_)
				{
					if (!current_frame_cloud_view_->cloud_viewer_.updatePointCloud (color_cloud, "OpenNICloud"))
					{
						current_frame_cloud_view_->cloud_viewer_.addPointCloud (color_cloud, "OpenNICloud");
						//current_frame_cloud_view_->cloud_viewer_.resetCameraViewpoint ("OpenNICloud");
					}    
					
					current_frame_cloud_view_->cloud_viewer_.spinOnce(1);
				}else
				{
					if( color_ )
					{
						if( !show_normals_ )
						{
							if( show_segmentation_ )
							{
								if (!current_frame_cloud_view_->cloud_viewer_.updatePointCloud (colored_cloud, "OpenNICloud"))
								{
									current_frame_cloud_view_->cloud_viewer_.addPointCloud(colored_cloud,"OpenNICloud");
									//current_frame_cloud_view_->cloud_viewer_.resetCameraViewpoint ("OpenNICloud");
								}
							}else
							{
                if (!current_frame_cloud_view_->cloud_viewer_.updatePointCloud (color_cloud, "OpenNICloud"))
					      {
						      current_frame_cloud_view_->cloud_viewer_.addPointCloud (color_cloud, "OpenNICloud");
						      //current_frame_cloud_view_->cloud_viewer_.resetCameraViewpoint ("OpenNICloud");
					      }   
							}
						}else
						{
							//current_frame_cloud_view_->show(cloud_host_,normals_host_);
						}
					}else{
						if( compute_normals_ )
							current_frame_cloud_view_->show(cloud_host_,normals_host_);
						else
							current_frame_cloud_view_->show(cloud_host_);

					}
					current_frame_cloud_view_->cloud_viewer_.spinOnce(1);
				}
			}
			frameNumber++;
      //printf("frameNumber: %i \n",frameNumber);
    }   
  }
  
  void source_cb1(const boost::shared_ptr<openni_wrapper::DepthImage>& depth_wrapper)  
  {        
    {
      boost::mutex::scoped_lock lock(data_ready_mutex_);
      if (exit_)
          return;
      
      depth_.cols = depth_wrapper->getWidth();
      depth_.rows = depth_wrapper->getHeight();
      depth_.step = depth_.cols * depth_.elemSize();

      source_depth_data_.resize(depth_.cols * depth_.rows);
      depth_wrapper->fillDepthImageRaw(depth_.cols, depth_.rows, &source_depth_data_[0]);
      depth_.data = &source_depth_data_[0];     
    }
    data_ready_cond_.notify_one();
  }

  void source_cb2(const boost::shared_ptr<openni_wrapper::Image>& image_wrapper, const boost::shared_ptr<openni_wrapper::DepthImage>& depth_wrapper, float constant)
  {
    {
			boost::mutex::scoped_try_lock lock(data_ready_mutex_); 

			if (exit_ || !lock) 
				return; 
          
      // test timestamps
      //
      /*unsigned long td=depth_wrapper->getTimeStamp();
      unsigned long tc=image_wrapper->getTimeStamp();
      std::cout<<"depth:"<<depth_wrapper->getFrameID()<<" time:"<<td<<std::endl;
      std::cout<<"image:"<<image_wrapper->getFrameID()<<" time:"<<tc<<std::endl;
      std::cout<<frameNumber<<" dif on time:"<<((long) td-tc)<<std::endl;*/
      //++frameNumber;

      depth_.cols = depth_wrapper->getWidth();
      depth_.rows = depth_wrapper->getHeight();
      depth_.step = depth_.cols * depth_.elemSize();

      source_depth_data_.resize(depth_.cols * depth_.rows);
      depth_wrapper->fillDepthImageRaw(depth_wrapper->getWidth(), depth_wrapper->getHeight(), &source_depth_data_[0]);
      depth_.data = &source_depth_data_[0];      
      
      rgb24_.cols = image_wrapper->getWidth();
      rgb24_.rows = image_wrapper->getHeight();
      rgb24_.step = rgb24_.cols * rgb24_.elemSize(); 

      source_image_data_.resize(rgb24_.cols * rgb24_.rows);
      image_wrapper->fillRGB(image_wrapper->getWidth(), image_wrapper->getHeight(), (unsigned char*)&source_image_data_[0]);
			rgb24_.data = &source_image_data_[0]; 

			// test
			//rgb_buffer = boost::shared_ptr<unsigned char>( new unsigned char[ image_wrapper->getWidth() * image_wrapper->getHeight() * 3 ] );
			//image_wrapper->fillRGB (image_wrapper->getWidth(), image_wrapper->getHeight(), rgb_buffer.get(), image_wrapper->getWidth() * 3);

			// set depth intrinsics
			fx_ = constant; fy_=constant;

			if( !gpu_ && color_)
			{
				pcl::ScopeTime timer("toPointCloud XYZRGB");
				//cloud_host_ = convertToXYZPointCloud(depth_wrapper,"prueba",constant);
				color_cloud_host_ = convertToXYZRGBPointCloud(image_wrapper,depth_wrapper,"prueba",constant);
			}
    }
    data_ready_cond_.notify_one();
  }

  void source_cb1_oni(const boost::shared_ptr<openni_wrapper::DepthImage>& depth_wrapper)  
  {        
    {
      boost::mutex::scoped_lock lock(data_ready_mutex_);
      if (exit_)
          return;
      
      depth_.cols = depth_wrapper->getWidth();
      depth_.rows = depth_wrapper->getHeight();
      depth_.step = depth_.cols * depth_.elemSize();

      source_depth_data_.resize(depth_.cols * depth_.rows);
      depth_wrapper->fillDepthImageRaw(depth_.cols, depth_.rows, &source_depth_data_[0]);
      depth_.data = &source_depth_data_[0];     
    }
    data_ready_cond_.notify_one();
  }

  void source_cb2_oni(const boost::shared_ptr<openni_wrapper::Image>& image_wrapper, const boost::shared_ptr<openni_wrapper::DepthImage>& depth_wrapper, float constant)
  {
    {
      boost::mutex::scoped_lock lock(data_ready_mutex_);
      if (exit_)
          return;
                  
      depth_.cols = depth_wrapper->getWidth();
      depth_.rows = depth_wrapper->getHeight();
      depth_.step = depth_.cols * depth_.elemSize();

      source_depth_data_.resize(depth_.cols * depth_.rows);
      depth_wrapper->fillDepthImageRaw(depth_.cols, depth_.rows, &source_depth_data_[0]);
      depth_.data = &source_depth_data_[0];      
      
      rgb24_.cols = image_wrapper->getWidth();
      rgb24_.rows = image_wrapper->getHeight();
      rgb24_.step = rgb24_.cols * rgb24_.elemSize(); 

      source_image_data_.resize(rgb24_.cols * rgb24_.rows);
      //image_wrapper->fillRGB(rgb24_.cols, rgb24_.rows, (unsigned char*)&source_image_data_[0]);
      image_wrapper->fillRGB(image_wrapper->getWidth(), image_wrapper->getHeight(), (unsigned char*)&source_image_data_[0]);
      rgb24_.data = &source_image_data_[0];      

      // set depth intrinsics
			fx_ = constant; fy_=constant;
    }
    data_ready_cond_.notify_one();
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  void
  startMainLoop (bool triggered_capture)
  {   
    using namespace openni_wrapper;
    typedef boost::shared_ptr<DepthImage> DepthImagePtr;
    typedef boost::shared_ptr<Image> ImagePtr;
        
    boost::function<void (const ImagePtr&, const DepthImagePtr&, float constant)> func1_dev = boost::bind (&KinectApp::source_cb2, this, _1, _2, _3);
    boost::function<void (const DepthImagePtr&)> func2_dev = boost::bind (&KinectApp::source_cb1, this, _1);

    boost::function<void (const ImagePtr&, const DepthImagePtr&, float constant)> func1_oni = boost::bind (&KinectApp::source_cb2_oni, this, _1, _2, _3);
    boost::function<void (const DepthImagePtr&)> func2_oni = boost::bind (&KinectApp::source_cb1_oni, this, _1);
    
    bool is_oni = dynamic_cast<pcl::ONIGrabber*>(&capture_) != 0;
    boost::function<void (const ImagePtr&, const DepthImagePtr&, float constant)> func1 = is_oni ? func1_oni : func1_dev;
    boost::function<void (const DepthImagePtr&)> func2 = is_oni ? func2_oni : func2_dev;

		bool need_colors = integrate_colors_;// || registration_;
		
    if( is_oni )
    {
      readKinFuTransformations();
      readGroundTruthPosition("C:\\Users\\Sergio\\Downloads\\kinect-video-experiments\\light.txt");
    }

		if( offline_ ) //file
		{
			try {
				offline_cloud_ = pcl::PointCloud<pcl::PointXYZRGB>::Ptr( new pcl::PointCloud<pcl::PointXYZRGB>());
				offline_cloud_->resize(rows_*cols_);

				pcl::io::loadPLYFile<pcl::PointXYZRGB>(offline_path_ + "cloud_" + offline_filename_ + ".ply",*offline_cloud_);
				readGroundTruthPosition(offline_path_ + "light.txt");

        // TODO: remove, only for rendering purposes
        readCPULightEstimations(offline_path_ + "cpuEstimation.txt");
        readGPULightEstimations(offline_path_ + "gpuEstimation.txt");

				//readRgbImage(offline_path_ + "rgb_image_" + offline_filename_ + ".png");
				for(int i=0;i<offline_cloud_->size();i++)
					if( offline_cloud_->points[i].x == 0 && offline_cloud_->points[i].y == 0 && offline_cloud_->points[i].z == 0 )
					{
						offline_cloud_->points[i].x = offline_cloud_->points[i].y = offline_cloud_->points[i].z = std::numeric_limits<float>::quiet_NaN();
					}
				offline_cloud_->width = cols_;
				offline_cloud_->height = rows_;
				this->execute (depth_, rgb24_, cloud_host_, normals_host_, true);

				if( !batch_ )
				{
					while (!exit_ && !current_frame_cloud_view_->cloud_viewer_.wasStopped () && !images_view_.viewerDepth_.wasStopped())
					{ 
						current_frame_cloud_view_->cloud_viewer_.spinOnce();
						images_view_.viewerColor_.spinOnce();
						images_view_.viewerDepth_.spinOnce();
						images_view_.viewerMono_.spinOnce();
						images_view_.viewerGeneratedMono_.spinOnce();
					}
				}
			}
			catch (const std::bad_alloc& /*e*/) { cout << "Bad alloc" << endl;
			}
      catch (const std::exception& /*e*/) { cout << "Exception" << endl;
		}

		}else //kinect
		{
			boost::signals2::connection c = need_colors ? capture_.registerCallback (func1) : capture_.registerCallback (func2);
      boost::unique_lock<boost::mutex> lock(data_ready_mutex_);

      if (!triggered_capture)
          capture_.start (); // Start stream
          
			while (!exit_ && !current_frame_cloud_view_->cloud_viewer_.wasStopped () && !images_view_.viewerDepth_.wasStopped())
      { 
        if (triggered_capture)
            capture_.start(); // Triggers new frame
        bool has_data = data_ready_cond_.timed_wait (lock, boost::posix_time::millisec(100));
                       
				try { this->execute (depth_, rgb24_, cloud_host_, normals_host_, has_data); }
        catch (const std::bad_alloc& /*e*/) { cout << "Bad alloc" << endl; break; }
        catch (const std::exception& /*e*/) { cout << "Exception" << endl; break; }                     
      }
      
      if (!triggered_capture)     
          capture_.stop (); // Stop stream

			c.disconnect();
		}
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  void
  writeCloud (int format) const
  {      
		if(!color_cloud->empty()) 
    {
			writeCloudFile (format, *color_cloud); 
		}
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  void
  printHelp ()
  {
    cout << endl;
    cout << "Kinect Ligh Estimation app hotkeys" << endl;
    cout << "=================" << endl;
    cout << "    H    : print this help" << endl;
    cout << "   Esc   : exit" << endl;   
    cout << "   1,2,3 : save cloud to PCD(binary), PCD(ASCII), PLY(ASCII)" << endl;
    cout << "    7,8  : save mesh to PLY, VTK" << endl;
    cout << endl;
  }  

  bool exit_;
	bool gpu_;
	
	bool offline_;
	bool compute_normals_;
	bool compute_surface_;
	bool planar_segmentation_;

  bool independent_camera_;

	bool record_;
	bool color_;
	bool show_normals_;
	bool show_segmentation_;
  bool showBoth;

  bool integrate_colors_;  
  float focal_length_;

	// depth intrinsics
	float fx_;
	float fy_;

	int rows_;
	int cols_;
  
  pcl::Grabber& capture_;
	int frameNumber;
	int screenshotCounter;
	float gridSize_x;
	float gridSize_y;
	float gridSize_z;
	float incr_x;
	float incr_y;
	float incr_z;

	// viewers
  //SceneCloudView scene_cloud_view_;
  ImageView images_view_;
  boost::shared_ptr<CurrentFrameCloudView> current_frame_cloud_view_;
  //pcl::visualization::CloudViewer viewer;

	// 
  boost::mutex data_ready_mutex_;
  boost::condition_variable data_ready_cond_;
 
	// memory for allocate depth and rgb pixels
  std::vector<PixelRGB> source_image_data_;
	std::vector<pcl::device::PixelHSV> source_hsv_image_data_;
  std::vector<unsigned short> source_depth_data_;
	std::vector<unsigned short> filtered_depth_data_;
	std::vector<float> source_grayscale_image_data_;
	std::vector<float> reconstructed_image_data_;

	// graysacale conversion type: 0,1,2
	int grayscale_conversion_type_;
	float color_segmentation_threshold_;
	float factor_segmentation_threshold_;

	// pointer to memory with extra information, ncol, nrow, size
  PtrStepSz<const unsigned short> depth_;
  PtrStepSz<const PixelRGB> rgb24_;

	DeviceArray<RGB> point_colors_device_; 
  PointCloud<RGB>::Ptr point_colors_ptr_;
	boost::shared_ptr<unsigned char> rgb_buffer;

	DepthMap depth_device_;
	DepthMap depth_device_bilateral_;
	DepthMap depth_device_median_;

	View rgb_device_;
	View rgb_bilateral_device_;
	DeviceArray2D<pcl::device::PixelHSV> hsv_device_;
	DeviceArray2D<float> grayscale_device_;

	pcl::gpu::DeviceArray2D<float> vmap_device;
	pcl::gpu::DeviceArray2D<float> nmap_device;
	pcl::gpu::DeviceArray<uint3> polygons_device;
	pcl::gpu::DeviceArray2D<pcl::device::float8> distances_graph_device_;
	pcl::gpu::DeviceArray2D<pcl::device::float8> color_d_graph_device_;
	pcl::gpu::DeviceArray2D<pcl::device::float8> normal_angles_graph_device_;

	///////////////////////////////////////////////////////////
	//		Kinect PointLight Estimation data structures
	//
	///////////////////////////////////////////////////////////
	// Device memory
	pcl::gpu::DeviceArray2D<float> distances_pointLight_;
	pcl::gpu::DeviceArray2D<float3> directions_pointLight_;
	pcl::gpu::DeviceArray2D<float> distances_pointView_;
	pcl::gpu::DeviceArray2D<float3> directions_pointView_;
	pcl::gpu::DeviceArray2D<float> attenuation_;
	pcl::gpu::DeviceArray2D<float> reflectance_;
	pcl::gpu::DeviceArray2D<float> albedo_;
	pcl::gpu::DeviceArray2D<float> mean_distances_;
	pcl::gpu::DeviceArray2D<float> mean_angles_;
	pcl::gpu::DeviceArray2D<float> mean_colors_;
	
	// Host memory
	pcl::PointCloud<float> distances_pointLight_host_;
	pcl::PointCloud<float> distances_pointView_host_;
	pcl::PointCloud<float3> directions_pointLight_host_;
	pcl::PointCloud<float3> directions_pointView_host_;
	pcl::PointCloud<float> attenuation_host_;
	pcl::PointCloud<float> reflectance_host_;
	pcl::PointCloud<float> albedo_host_;
	pcl::PointCloud<float> mean_distances_host_;
	pcl::PointCloud<float> mean_angles_host_;
	pcl::PointCloud<float> mean_colors_host_;

	float3 minPointLight_;
	float3 gtMinPointLight_;
  float3 gtMinPointLightFrame0_;

	std::string offline_filename_;
	std::string offline_path_;

	pcl::PointCloud<pcl::PointXYZRGB>::Ptr offline_cloud_;
	
	boost::shared_ptr<std::vector<pcl::PointIndices>> segments;

	int lineNumber;
	int maxNumPassesBilateral_;
	bool batch_;
	///////////////////////////////////////////////////////////

	// CPU VERSION
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr output_filtered_cloud_;

	////////////////

	pcl::gpu::DeviceArray2D<pXYZ> organized_points_device_;
	pcl::gpu::DeviceArray2D<pcl::Normal> organized_normals_device_;

	pcl::PointCloud<pcl::PointXYZRGB>::Ptr color_cloud_host_;
	pcl::PointCloud<pXYZ>::Ptr cloud_host_;
	pcl::PointCloud<nXYZ>::Ptr normals_host_;
	pcl::PointCloud<pcl::PointXYZRGBA>::Ptr color_cloud;
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr transformed_cloud;
	pcl::PointCloud<pcl::PointXYZRGBA>::Ptr colored_cloud;

	pcl::PointCloud<pcl::device::float8>::Ptr distances_graph_host_;
	pcl::PointCloud<pcl::device::float8>::Ptr color_d_graph_host_;
	pcl::PointCloud<pcl::device::float8>::Ptr normal_angles_graph_host_;
	pcl::PointCloud<float>::Ptr distances_light_host_;
	pcl::PointCloud<float>::Ptr distances_pointview_host_;

	// 3D reconstruction
	pcl::OrganizedFastMesh<pXYZ> ofm;
	pcl::PolygonMesh::Ptr mesh_;
	boost::shared_ptr<std::vector<pcl::Vertices> > vertices_;
	boost::shared_ptr<std::vector<uint3> > vertices_device_;

	pcl::IntegralImageNormalEstimation<pcl::PointXYZRGB, nXYZ> ne;
	pcl::ExtractIndices<pXYZ> extract_;

	std::vector<pcl::ModelCoefficients, std::allocator<pcl::ModelCoefficients > > coefficients;
	std::vector<pcl::PointIndices, std::allocator<pcl::PointIndices > > indices;

  // kinfu camera transformations
  std::vector< Trans > trajectories_;
  std::string trajectories_file_;

  int time_ms_;

  pcl::PointXYZ light_source_pos_cpu_;
  pcl::PointXYZ light_source_pos_gpu_;

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  static void
  keyboard_callback (const visualization::KeyboardEvent &e, void *cookie)
  {
    KinectApp* app = reinterpret_cast<KinectApp*> (cookie);

    int key = e.getKeyCode ();

    if (e.keyUp ())    
      switch (key)
      {
				case 27: app->exit_ = true; break;
        case (int)'b': case (int)'B': app->toggleShowBoth (); break;
				case (int)'h': case (int)'H': app->printHelp (); break;
				case (int)'n': case (int)'N': app->toggleShowNormals (); break;  
				case (int)'s': case (int)'S': app->toggleSurface (); break;
				case (int)'c': case (int)'C': app->toggleShowSegmentation (); break;
				case (int)'r': case (int)'R': app->resetPointLightSource (); break;
				case (int)'p': case (int)'P': app->togglePlanarSegmentation (); break;
				case (int)'1': case (int)'2': case (int)'3': app->writeCloud (key - (int)'0'); break;    
				case (int)'w': case (int)'W': app->writeScene(); break; 
				case (int)'5': app->writeSceneSegmentation (); break; 
				case (int)'6': app->writeSceneScreenshot (); break; 
				case (int)'t': case (int)'T': app->toggleConversionType(); break;
				case (int)'q': case (int)'Q': app->toggleColor(); break;
				//case (int)'2': app->increaseColorSegmentationThreshold (); break; 
				//case (int)'1': app->decreaseColorSegmentationThreshold (); break;
				case (int)'4': app->increaseFactorDistanceSegmentationThreshold (); break; 
				//case (int)'3': app->decreaseFactorDistanceSegmentationThreshold (); break;
				case (int)'x': case (int)'X': break;
				case (int)'v': case (int)'V': break;
				default:
					break;
      }    
  }
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<typename CloudPtr> void
writeCloudFile (int format, const CloudPtr& cloud_prt)
{
  if (format == KinectApp::PCD_BIN)
  {
    cout << "Saving point cloud to 'cloud_bin.pcd' (binary)... " << flush;
    pcl::io::savePCDFile ("cloud_bin.pcd", cloud_prt, true);
  }
  else
  if (format == KinectApp::PCD_ASCII)
  {
    cout << "Saving point cloud to 'cloud.pcd' (ASCII)... " << flush;
    pcl::io::savePCDFile ("cloud.pcd", cloud_prt, false);
  }
  else   /* if (format == KinFuApp::PLY) */
  {
    cout << "Saving point cloud to 'cloud.ply' (ASCII)... " << flush;
    //pcl::io::savePLYFileASCII ("cloud.ply", *cloud_prt);
		//pcl::io::savePLYFileBinary ("cloud.ply",*cloud_prt);
  }
  cout << "Done" << endl;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void
writePoligonMeshFile (int format, const pcl::PolygonMesh& mesh)
{
  if (format == KinectApp::MESH_PLY)
  {
    cout << "Saving mesh to to 'mesh.ply'... " << flush;
    pcl::io::savePLYFile("mesh.ply", mesh);		
  }
  else /* if (format == KinectApp::MESH_VTK) */
  {
    cout << "Saving mesh to to 'mesh.vtk'... " << flush;
    pcl::io::saveVTKFile("mesh.vtk", mesh);    
  }  
  cout << "Done" << endl;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int
print_cli_help ()
{
  cout << "\nKinect Light Estimation parameters:" << endl;
  cout << "    --help, -h                      : print this message" << endl;  
  cout << "    -path,                          : specify absolute path to the input data" << endl;
  cout << "    -fileNumber,                    : specify number of the scene within the specified path" << endl;
  cout << "	   --gpu						   : compute onto the GPU "<< endl;
  cout << "	   -gpuDevice					   : specify GPU device number "<< endl;
  cout << "                                                               " << endl;
  cout << "example: kinectLightEstimation --gpu -path C:\KinectLightEstimation_Dataset_v1\exp1\ -fileNumber 1" << endl;
  return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int
main (int argc, char* argv[])
{  
  if (pc::find_switch (argc, argv, "--help") || pc::find_switch (argc, argv, "-h"))
    return print_cli_help ();
  
	int passesBilateral =	1;
  int device = 0;
	int filetype = 0; // when kinect false, 0 means pcd, 1 ply inputfile

  pc::parse_argument (argc, argv, "-gpuDevice", device);
   
  boost::shared_ptr<pcl::Grabber> capture;

  bool triggered_capture = false;
	bool gpu = false;
	bool kinect = true;
	bool batch = false;
  
  std::string eval_folder, match_file, openni_device, path, fileNumber, oni_file, kinfu_trajectories_file;
  try
  {   
		if (pc::find_switch (argc, argv, "--gpu"))
			gpu=true;

		if (pc::find_switch (argc, argv, "--batch"))
			batch=true;

		pc::parse_argument(argc, argv, "-passesBilateral", passesBilateral);
		pc::parse_argument(argc, argv, "-gpuDevice", device);

    if (pc::parse_argument (argc, argv, "-dev", openni_device) > 0)
    {
      capture.reset (new pcl::OpenNIGrabber (openni_device));
    }
    else if (pc::parse_argument (argc, argv, "-oni", oni_file) > 0)
    {
      pc::parse_argument (argc, argv, "-trajectories", kinfu_trajectories_file);

      triggered_capture = true;
      bool repeat = false; // Only run ONI file once
      capture.reset (new pcl::ONIGrabber (oni_file, repeat, ! triggered_capture));
    }
		else if(pc::parse_argument(argc, argv, "-path", path) > 0)
		{
			//load pcd file
      pc::parse_argument(argc, argv, "-fileNumber", fileNumber);
			kinect=false;
			filetype=1;
		}
    else
    {
      capture.reset( new pcl::OpenNIGrabber() );
    }
  }
  catch (const pcl::PCLException& /*e*/) { return cout << "Can't open depth source" << endl, -1; }

	if(gpu)
	{
		pcl::gpu::setDevice (device);
		pcl::gpu::printShortCudaDeviceInfo (device);
	}

	{
		if(!kinect)
			capture.reset (new pcl::PCDGrabber<pXYZ> ("", 15.0, false));

		KinectApp app(*capture, 0.0, gpu, !kinect, path, fileNumber, passesBilateral, batch, kinfu_trajectories_file);

		app.initCurrentFrameView ();
		// executing
		try { app.startMainLoop (triggered_capture); }  
		catch (const pcl::PCLException& /*e*/) { cout << "PCLException" << endl; }
		catch (const std::bad_alloc& /*e*/) { cout << "Bad alloc" << endl; }
		catch (const std::exception& /*e*/) { cout << "Exception" << endl; }
		return 0;
	}
}

////////////////////////////////////////