#include <algorithm>
#include <iostream>
#include <vector>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/search/pcl_search.h>
#include <pcl/search/kdtree.h>
#include "../include/internal.h"

void
bilateralFilter_CPU(const std::vector<unsigned short> &input_depth, std::vector<unsigned short> &output_depth,const float sigma_space2_inv_half,const float sigma_color2_inv_half,const int width,const int height)
{
	const int R = 6;       //static_cast<int>(sigma_space * 1.5);
	const int D = R * 2 + 1;

	for(int y=0;y<height;y++)
		for(int x=0; x<width;x++)
		{

      int value = input_depth[y*width+x];
			
			int tx = std::min (x - D / 2 + D, width - 1);
			int ty = std::min (y - D / 2 + D, height - 1);

			float sum1 = 0;
			float sum2 = 0;

			for (int cy = std::max (y - D / 2, 0); cy < ty; ++cy)
			{
				for (int cx = std::max (x - D / 2, 0); cx < tx; ++cx)
				{
					int tmp = input_depth[y*width+x];

					float space2 = (x - cx) * (x - cx) + (y - cy) * (y - cy);
					float color2 = (value - tmp) * (value - tmp);

					float weight = expf (-(space2 * sigma_space2_inv_half + color2 * sigma_color2_inv_half));

					sum1 += tmp * weight;
					sum2 += weight;
				}
			}

			int res = (int) ((sum1 / sum2)+1);
			output_depth[y*width+x] = std::min (res, (int)std::numeric_limits<unsigned short>::max ());
		}
}

void 
computeSegmentationGraphCPU(const pcl::PointCloud<pcl::PointXYZRGB> &color_cloud, pcl::PointCloud<pcl::device::float8> &distance_map,
	pcl::PointCloud<pcl::device::float8> &color_d_map, pcl::PointCloud<pcl::device::float8> &normals_angle_map)
{
	int idx,left,right,up,down,up_left,up_right,down_left,down_right;
	int width = color_cloud.width;
	int height = color_cloud.height; 

	for(int y=0;y<color_cloud.height;y++)
		for(int x=0;x<color_cloud.width;x++)
		{
			// if isnan next point
			if( !pcl::isFinite( color_cloud.points[y*width+x] ) )
				continue;

			// calculate index neighborhood
			up_left = (y-1) * width + (x-1);
			up = (y-1) * width + x;
			up_right = (y-1) * width + (x+1);
			left = y * width + (x-1);
			idx = y * width + x;
			right = y * width + (x+1);
			down_left = (y+1) * width + (x-1);
			down = (y+1) * width + x;
			down_right = (y+1) * width + (x+1);
			
			Eigen::Vector3f p_idx = color_cloud.points[idx].getVector3fMap();
			Eigen::Vector3i c_idx = color_cloud.points[idx].getRGBVector3i();

			// up-left
			if( y-1 >= 0 && x-1 >= 0 )
			{
				if( pcl::isFinite(color_cloud.points[up_left]) )
				{
					Eigen::Vector3f p_up_left = color_cloud.points[up_left].getVector3fMap();
					distance_map[idx].x = (p_idx - p_up_left).norm();
					Eigen::Vector3i c_up_left = color_cloud.points[up_left].getRGBVector3i();
					Eigen::Vector3i aux = (c_idx - c_up_left);
					color_d_map[idx].x = sqrtf(aux.dot(aux));
				}else
				{
					distance_map[idx].x = std::numeric_limits<float>::quiet_NaN();
					color_d_map[idx].x = std::numeric_limits<float>::quiet_NaN();
				}
			}else
			{
				distance_map[idx].x = std::numeric_limits<float>::quiet_NaN();
				color_d_map[idx].x = std::numeric_limits<float>::quiet_NaN();
			}

			// up
			if( y-1 >= 0 )
			{
				if( pcl::isFinite(color_cloud.points[up]) )
				{
					Eigen::Vector3f p_up = color_cloud.points[up_left].getVector3fMap();
					distance_map[idx].y = (p_idx - p_up).norm();
					Eigen::Vector3i c_up = color_cloud.points[up].getRGBVector3i();
					Eigen::Vector3i aux = (c_idx - c_up);
					color_d_map[idx].y = sqrtf(aux.dot(aux));
				}else
				{
					distance_map[idx].y = std::numeric_limits<float>::quiet_NaN();
					color_d_map[idx].y = std::numeric_limits<float>::quiet_NaN();
				}
			}else
			{
				distance_map[idx].y = std::numeric_limits<float>::quiet_NaN();
				color_d_map[idx].y = std::numeric_limits<float>::quiet_NaN();
			}

			// up-right
			if( y-1 >= 0 && x+1 < width )
			{
				if( pcl::isFinite(color_cloud.points[up_right]) )
				{
					Eigen::Vector3f p_up_right = color_cloud.points[up_right].getVector3fMap();
					distance_map[idx].z = (p_idx - p_up_right).norm();
					Eigen::Vector3i c_up_right = color_cloud.points[up_right].getRGBVector3i();
					Eigen::Vector3i aux = (c_idx - c_up_right);
					color_d_map[idx].z = sqrtf(aux.dot(aux));
				}else
				{
					distance_map[idx].z = std::numeric_limits<float>::quiet_NaN();
					color_d_map[idx].z = std::numeric_limits<float>::quiet_NaN();
				}
			}else
			{
				distance_map[idx].z = std::numeric_limits<float>::quiet_NaN();
				color_d_map[idx].z = std::numeric_limits<float>::quiet_NaN();
			}

			// left
			if( x-1 >= 0 )
			{
				if( pcl::isFinite(color_cloud.points[left]) )
				{
					Eigen::Vector3f p_left = color_cloud.points[left].getVector3fMap();
					distance_map[idx].w = (p_idx - p_left).norm();
					Eigen::Vector3i c_left = color_cloud.points[left].getRGBVector3i();
					Eigen::Vector3i aux = (c_idx - c_left);
					color_d_map[idx].w = sqrtf(aux.dot(aux));
				}else
				{
					distance_map[idx].w = std::numeric_limits<float>::quiet_NaN();
					color_d_map[idx].w = std::numeric_limits<float>::quiet_NaN();
				}
			}else
			{
				distance_map[idx].w = std::numeric_limits<float>::quiet_NaN();
				color_d_map[idx].w = std::numeric_limits<float>::quiet_NaN();
			}

			// right
			if( x+1 < width )
			{
				if( pcl::isFinite(color_cloud.points[right]) )
				{
					Eigen::Vector3f p_right = color_cloud.points[right].getVector3fMap();
					distance_map[idx].c1 = (p_idx - p_right).norm();
					Eigen::Vector3i c_right = color_cloud.points[right].getRGBVector3i();
					Eigen::Vector3i aux = (c_idx - c_right);
					color_d_map[idx].c1 = sqrtf(aux.dot(aux));
				}else
				{
					distance_map[idx].c1 = std::numeric_limits<float>::quiet_NaN();
					color_d_map[idx].c1 = std::numeric_limits<float>::quiet_NaN();
				}
			}else
			{
				distance_map[idx].c1 = std::numeric_limits<float>::quiet_NaN();
				color_d_map[idx].c1 = std::numeric_limits<float>::quiet_NaN();
			}

			// down left
			if( x-1 >= 0 && y+1 < height )
			{
				if( pcl::isFinite(color_cloud.points[down_left]) )
				{
					Eigen::Vector3f p_down_left = color_cloud.points[down_left].getVector3fMap();
					distance_map[idx].c2 = (p_idx - p_down_left).norm();
					Eigen::Vector3i c_down_left = color_cloud.points[down_left].getRGBVector3i();
					Eigen::Vector3i aux = (c_idx - c_down_left);
					color_d_map[idx].c2 = sqrtf(aux.dot(aux));
				}else
				{
					distance_map[idx].c2 = std::numeric_limits<float>::quiet_NaN();
					color_d_map[idx].c2 = std::numeric_limits<float>::quiet_NaN();
				}
			}else
			{
				distance_map[idx].c2 = std::numeric_limits<float>::quiet_NaN();
				color_d_map[idx].c2 = std::numeric_limits<float>::quiet_NaN();
			}

			// down
			if( y+1 < height )
			{
				if( pcl::isFinite(color_cloud.points[down]) )
				{
					Eigen::Vector3f p_down = color_cloud.points[down].getVector3fMap();
					distance_map[idx].c3 = (p_idx - p_down).norm();
					Eigen::Vector3i c_down = color_cloud.points[down].getRGBVector3i();
					Eigen::Vector3i aux = (c_idx - c_down);
					color_d_map[idx].c3 = sqrtf(aux.dot(aux));
				}else
				{
					distance_map[idx].c3 = std::numeric_limits<float>::quiet_NaN();
					color_d_map[idx].c3 = std::numeric_limits<float>::quiet_NaN();
				}
			}else
			{
				distance_map[idx].c3 = std::numeric_limits<float>::quiet_NaN();
				color_d_map[idx].c3 = std::numeric_limits<float>::quiet_NaN();
			}

			// down right
			if( y+1 < height && x+1 < width )
			{
				if( pcl::isFinite(color_cloud.points[down_right]) )
				{
					Eigen::Vector3f p_down_right = color_cloud.points[down_right].getVector3fMap();
					distance_map[idx].c4 = (p_idx - p_down_right).norm();
					Eigen::Vector3i c_down_right = color_cloud.points[down_right].getRGBVector3i();
					Eigen::Vector3i aux = (c_idx - c_down_right);
					color_d_map[idx].c4 = sqrtf(aux.dot(aux));
				}else
				{
					distance_map[idx].c4 = std::numeric_limits<float>::quiet_NaN();
					color_d_map[idx].c4 = std::numeric_limits<float>::quiet_NaN();
				}
			}else
			{
				distance_map[idx].c4 = std::numeric_limits<float>::quiet_NaN();
				color_d_map[idx].c4 = std::numeric_limits<float>::quiet_NaN();
			}
		}
}

void
computeDistancesMapCPU (const pcl::PointCloud<pcl::PointXYZRGB> &cloud, const float3 point, pcl::PointCloud<float> &distances_map, pcl::PointCloud<float3> &normalized_directions, int rows, int cols)
{
	for(int y=0;y<rows;y++)
		for(int x=0;x<cols;x++)
		{
			pcl::PointXYZRGB pt = cloud.points[y*cols+x];

			if( !pcl::isFinite(pt) )
			{
				distances_map.points[y*cols+x] = std::numeric_limits<float>::quiet_NaN();
				normalized_directions[y*cols+x].x = normalized_directions[y*cols+x].y = normalized_directions[y*cols+x].z = std::numeric_limits<float>::quiet_NaN();
				continue;
			}else
			{
				Eigen::Vector3f p; p(0) = point.x; p(1) = point.y; p(2) = point.z;
				Eigen::Vector3f query_point = pt.getVector3fMap();
				distances_map.points[y*cols+x] = (p-query_point).norm();

				Eigen::Vector3f direction = (p-query_point).normalized();
				normalized_directions[y*cols+x].x = direction(0); 
				normalized_directions[y*cols+x].y = direction(1); 
				normalized_directions[y*cols+x].z = direction(2); 
			}
		}
}

void
computeAttenuationReflectanceAlbedoMapsCPU(const pcl::PointCloud<float> &dmap_pointLight,const pcl::PointCloud<float> &dmap_pointView, pcl::PointCloud<float> &attenuation_map, 
			const pcl::PointCloud<pcl::Normal> &nmap, const pcl::PointCloud<float3> &directions_pointLight, pcl::PointCloud<float> &reflectance_map, const float attenuationFactor, 
			const std::vector<float> &grayscaleImage,	pcl::PointCloud<float> &albedo_map, const int rows, const int cols)
{
	
	float bad_point = std::numeric_limits<float>::quiet_NaN ();

	for(int y=0;y<rows;y++)
		for(int x=0;x<cols;x++)
		{
			float dv_pointLight = dmap_pointLight.points[y*cols + x];
			float dv_pointView = dmap_pointView.points[y*cols + x];

			if( !pcl_isfinite(dv_pointLight) || !pcl_isfinite(dv_pointView) )
			{
				attenuation_map.points[y*cols + x] = bad_point;
				reflectance_map.points[y*cols + x] = bad_point;
				albedo_map.points[y*cols + x] = bad_point;
				continue;
			}
				
			// Att = exp((-k.*DistToViewer)./(DistToLight.*DistToLight));
			attenuation_map.points[y*cols + x] = exp((-attenuationFactor*dv_pointView)/(dv_pointLight*dv_pointLight));
			
			// Reflectance calculus
			if( !pcl::isFinite(nmap.points[y*cols + x]) )
			{
				reflectance_map.points[y*cols + x] = bad_point;
				albedo_map.points[y*cols + x] = bad_point;
				continue;
			}

			Eigen::Vector3f normal_p = nmap.points[y*cols + x].getNormalVector3fMap();

			float3 aux = directions_pointLight.points[y*cols+x];
			Eigen::Vector3f direction_p; direction_p(0) = aux.x; direction_p(1) = aux.y; direction_p(2) = aux.z;

			float reflectance = attenuation_map.points[y*cols+x] * normal_p.dot(direction_p);
			// remove shadows: reflectance < 0
			if( reflectance < 0 ){
				reflectance_map.points[y*cols+x] = 0;
				albedo_map.points[y*cols+x] = 0;
			}else
			{
				reflectance_map.points[y*cols+x] = reflectance;
				//calculate albedo factor
				albedo_map.points[y*cols+x] = grayscaleImage[y*cols+x] / reflectance; 
			}
		}
}

#define MIN3(x,y,z)  ((y) <= (z) ? \
                         ((x) <= (y) ? (x) : (y)) \
                     : \
                         ((x) <= (z) ? (x) : (z)))

		#define MAX3(x,y,z)  ((y) >= (z) ? \
														 ((x) >= (y) ? (x) : (y)) \
												 : \
														 ((x) >= (z) ? (x) : (z)))

void
convertRGBtoGrayKernel (int rows, int cols, const pcl::PointCloud<pcl::PointXYZRGB> &cmap, std::vector<float> &gray_map, const int conversion_type)
{
	for(int y=0;y<rows;y++)
		for(int x=0;x<cols;x++)
		{
			pcl::PointXYZRGB rgb = cmap.points[y*cols+x];
				
			// average
			if( conversion_type == 0)
			{
				gray_map[y*cols+x] = (rgb.r + rgb.g + rgb.b)/3;
			}
			// lighting
			else if( conversion_type == 1)
			{
				gray_map[y*cols+x] = (MAX3(rgb.r, rgb.g, rgb.b) + MIN3(rgb.r, rgb.g, rgb.b))/2;
			}
			// luminosity
			else
			{
				gray_map[y*cols+x] = (0.21f * rgb.r + 0.71f * rgb.g + 0.07f * rgb.b);
			}
			gray_map[y*cols+x] /= 255;
		}
}

double computeCloudResolution (const pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr &cloud) 
{ 
  double res = 0.0; 
  int n_points = 0; 
  int nres; 
  std::vector<int> indices (2); 
  std::vector<float> sqr_distances (2); 
  pcl::search::KdTree<pcl::PointXYZRGB> tree; 
  tree.setInputCloud (cloud); 

  for (size_t i = 0; i < cloud->size (); ++i) 
  { 
      if (! pcl_isfinite ((*cloud)[i].x)) 
      { 
        continue; 
      } 
      //Considering the second neighbor since the first is the point itself. 
      nres = tree.nearestKSearch (i, 2, indices, sqr_distances); 
      if (nres == 2) 
      { 
        res += sqrt (sqr_distances[1]); 
        ++n_points; 
      } 
  } 

  if (n_points != 0) 
  { 
      res /= n_points; 
  } 

  return res; 
}


