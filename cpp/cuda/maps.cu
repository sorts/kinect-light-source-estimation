/*
 * Software License Agreement (BSD License)
 *
 *  Point Cloud Library (PCL) - www.pointclouds.org
 *  Copyright (c) 2011, Willow Garage, Inc.
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "device.hpp"

using namespace pcl::device;
using namespace pcl::gpu;

namespace pcl
{
  namespace device
  {
    __global__ void
    computeVmapKernel (const PtrStepSz<unsigned short> depth, PtrStep<float> vmap, float fx_inv, float fy_inv, float cx, float cy)
    {
      int u = threadIdx.x + blockIdx.x * blockDim.x;
      int v = threadIdx.y + blockIdx.y * blockDim.y;

      if (u < depth.cols && v < depth.rows)
      {
        float z = depth.ptr (v)[u] / 1000.f; // load and convert: mm -> meters

        if (z != 0)
        {
          float vx = z * (u - cx) * fx_inv;
          float vy = z * (v - cy) * fy_inv;
          float vz = z;

          vmap.ptr (v                 )[u] = vx;
          vmap.ptr (v + depth.rows    )[u] = vy;
          vmap.ptr (v + depth.rows * 2)[u] = vz;
        }
        else
          vmap.ptr (v)[u] = numeric_limits<float>::quiet_NaN ();

      }
    }

    __global__ void
    computeNmapKernel (int rows, int cols, const PtrStep<float> vmap, PtrStep<float> nmap)
    {
      int u = threadIdx.x + blockIdx.x * blockDim.x;
      int v = threadIdx.y + blockIdx.y * blockDim.y;

      if (u >= cols || v >= rows)
        return;

      if (u == cols - 1 || v == rows - 1)
      {
        nmap.ptr (v)[u] = numeric_limits<float>::quiet_NaN ();
        return;
      }

      float3 v00, v01, v10;
      v00.x = vmap.ptr (v  )[u];
      v01.x = vmap.ptr (v  )[u + 1];
      v10.x = vmap.ptr (v + 1)[u];

      if (!isnan (v00.x) && !isnan (v01.x) && !isnan (v10.x))
      {
        v00.y = vmap.ptr (v + rows)[u];
        v01.y = vmap.ptr (v + rows)[u + 1];
        v10.y = vmap.ptr (v + 1 + rows)[u];

        v00.z = vmap.ptr (v + 2 * rows)[u];
        v01.z = vmap.ptr (v + 2 * rows)[u + 1];
        v10.z = vmap.ptr (v + 1 + 2 * rows)[u];

        float3 r = normalized (cross (v01 - v00, v10 - v00));

        nmap.ptr (v       )[u] = r.x;
        nmap.ptr (v + rows)[u] = r.y;
        nmap.ptr (v + 2 * rows)[u] = r.z;
      }
      else
        nmap.ptr (v)[u] = numeric_limits<float>::quiet_NaN ();
    }

		__global__ void
    computeSegmentationGraphKernel (int rows, int cols, const PtrStep<float> vmap, const PtrStep<pcl::gpu::PixelRGB> cmap, const PtrStep<float> nmap, 
					PtrStep<float8> sp_distance, PtrStep<float8> c_distance, PtrStep<float8> n_angles)
    {
			int u = threadIdx.x + blockIdx.x * blockDim.x;
      int v = threadIdx.y + blockIdx.y * blockDim.y;

			bool compute_distances=true;

      if (u >= cols || v >= rows)
        return;

			float3 center, leftup, up, rightup, left, right, leftdown, down, rightdown;
			pcl::gpu::PixelRGB c_center, c_leftup, c_up, c_rightup, c_left, c_right, c_leftdown, c_down, c_rightdown;
			float3 n_center, n_leftup, n_up, n_rightup, n_left, n_right, n_leftdown, n_down, n_rightdown;

			//center point
			center = make_float3( vmap.ptr (v)[u],vmap.ptr (v+rows)[u], vmap.ptr (v+2*rows)[u] );
			n_center = make_float3( nmap.ptr (v)[u], nmap.ptr (v+rows)[u], nmap.ptr (v+2*rows)[u] );

			if( isnan(center.x) || isnan(center.y) || isnan(center.z) )
				compute_distances=false;

			c_center = cmap.ptr(v)[u];

			//leftup point
			if( v-1 >= 0 && u-1 >= 0 ){
				if( compute_distances ){
					leftup = make_float3( vmap.ptr (v-1)[u-1],vmap.ptr (v+rows-1)[u-1], vmap.ptr (v+2*rows-1)[u-1] );
					n_leftup = make_float3( nmap.ptr (v-1)[u-1],nmap.ptr (v+rows-1)[u-1], nmap.ptr (v+2*rows-1)[u-1] );
					if( isnan(leftup.x) || isnan(leftup.y) || isnan(leftup.z) ){
						sp_distance.ptr(v)[u].x = numeric_limits<float>::quiet_NaN ();
						n_angles.ptr(v)[u].x = numeric_limits<float>::quiet_NaN ();
					}else{
						sp_distance.ptr(v)[u].x = norm(leftup - center);						
						n_angles.ptr(v)[u].x = dot(n_leftup,n_center)/(norm(n_leftup)*norm(n_center));
						n_angles.ptr(v)[u].x = acos( n_angles.ptr(v)[u].x );
						n_angles.ptr(v)[u].x = fabs( (n_angles.ptr(v)[u].x * 180) / 3.1416 ); 
					}
				}else
					leftup.x = leftup.y = leftup.z = sp_distance.ptr(v)[u].x = n_angles.ptr(v)[u].x = numeric_limits<float>::quiet_NaN ();

				c_leftup = cmap.ptr(v-1)[u-1];
				c_distance.ptr(v)[u].x = sqrtf((c_center.r-c_leftup.r)*(c_center.r-c_leftup.r) + (c_center.g-c_leftup.g)*(c_center.g-c_leftup.g) + (c_center.b-c_leftup.b)*(c_center.b-c_leftup.b));
			}else{
				leftup.x = leftup.y = leftup.z = sp_distance.ptr(v)[u].x = c_distance.ptr(v)[u].x = n_angles.ptr(v)[u].x = numeric_limits<float>::quiet_NaN ();
			}

			//up point
			if( v-1 >= 0 ){
				if( compute_distances )
				{
					up = make_float3( vmap.ptr (v-1)[u],vmap.ptr (v+rows-1)[u], vmap.ptr (v+2*rows-1)[u] );
					n_up = make_float3( nmap.ptr (v-1)[u],nmap.ptr (v+rows-1)[u], nmap.ptr (v+2*rows-1)[u] );
					if( isnan(up.x) || isnan(up.y) || isnan(up.z) ){
						sp_distance.ptr(v)[u].y = numeric_limits<float>::quiet_NaN ();
						n_angles.ptr(v)[u].y = numeric_limits<float>::quiet_NaN ();
					}else{
						sp_distance.ptr(v)[u].y = norm(up - center);
						n_angles.ptr(v)[u].y = dot(n_up,n_center)/(norm(n_up)*norm(n_center));
						n_angles.ptr(v)[u].y = acos( n_angles.ptr(v)[u].y );
						n_angles.ptr(v)[u].y = fabs( (n_angles.ptr(v)[u].y * 180) / 3.1416 ); 
					}
				}else
					up.x = up.y = up.z = sp_distance.ptr(v)[u].y = n_angles.ptr(v)[u].y = numeric_limits<float>::quiet_NaN ();

				c_up = cmap.ptr(v-1)[u];
				c_distance.ptr(v)[u].y = sqrtf((c_center.r-c_up.r)*(c_center.r-c_up.r) + (c_center.g-c_up.g)*(c_center.g-c_up.g) + (c_center.b-c_up.b)*(c_center.b-c_up.b));
			}else{
				up.x = up.y = up.z = sp_distance.ptr(v)[u].y = c_distance.ptr(v)[u].y = n_angles.ptr(v)[u].y = numeric_limits<float>::quiet_NaN ();
			}

			//rightup point
			if( v-1 >= 0 && u+1 < cols ){
				if( compute_distances )
				{
					rightup = make_float3( vmap.ptr (v-1)[u+1],vmap.ptr (v+rows-1)[u+1], vmap.ptr (v+2*rows-1)[u+1] );
					n_rightup = make_float3( nmap.ptr (v-1)[u+1],nmap.ptr (v+rows-1)[u+1], nmap.ptr (v+2*rows-1)[u+1] );
					if( isnan(rightup.x) || isnan(rightup.y) || isnan(rightup.z) ){
						sp_distance.ptr(v)[u].z = numeric_limits<float>::quiet_NaN ();
						n_angles.ptr(v)[u].z = numeric_limits<float>::quiet_NaN ();
					}else{
						sp_distance.ptr(v)[u].z = norm(rightup - center);
						n_angles.ptr(v)[u].z = dot(n_rightup,n_center)/(norm(n_rightup)*norm(n_center));
						n_angles.ptr(v)[u].z = acos( n_angles.ptr(v)[u].z );
						n_angles.ptr(v)[u].z = fabs( (n_angles.ptr(v)[u].z * 180) / 3.1416 ); 
					}
				}else
					rightup.x = rightup.y = rightup.z = sp_distance.ptr(v)[u].z = n_angles.ptr(v)[u].z = numeric_limits<float>::quiet_NaN ();

				c_rightup = cmap.ptr(v-1)[u+1];
				c_distance.ptr(v)[u].z = sqrtf((c_center.r-c_rightup.r)*(c_center.r-c_rightup.r) + (c_center.g-c_rightup.g)*(c_center.g-c_rightup.g) + (c_center.b-c_rightup.b)*(c_center.b-c_rightup.b));
			}else{
				rightup.x = rightup.y = rightup.z = sp_distance.ptr(v)[u].z = c_distance.ptr(v)[u].z = n_angles.ptr(v)[u].z = numeric_limits<float>::quiet_NaN ();
			}

			//left point
			if( u-1 >= 0 ){
				if( compute_distances )
				{
					left = make_float3( vmap.ptr (v)[u-1],vmap.ptr (v+rows)[u-1], vmap.ptr (v+2*rows)[u-1] );
					n_left = make_float3( nmap.ptr (v)[u-1],nmap.ptr (v+rows)[u-1], nmap.ptr (v+2*rows)[u-1] );
					if( isnan(left.x) || isnan(left.y) || isnan(left.z) ){
						sp_distance.ptr(v)[u].w = numeric_limits<float>::quiet_NaN ();
						n_angles.ptr(v)[u].w = numeric_limits<float>::quiet_NaN ();
					}else{
						sp_distance.ptr(v)[u].w = norm(left - center);
						n_angles.ptr(v)[u].w = dot(n_left,n_center)/(norm(n_left)*norm(n_center));
						n_angles.ptr(v)[u].w = acos( n_angles.ptr(v)[u].w );
						n_angles.ptr(v)[u].w = fabs( (n_angles.ptr(v)[u].w * 180) / 3.1416 ); 
					}
				}else
					left.x = left.y = left.z = sp_distance.ptr(v)[u].w = n_angles.ptr(v)[u].w = numeric_limits<float>::quiet_NaN ();

				c_left = cmap.ptr(v)[u-1];
				c_distance.ptr(v)[u].w = sqrtf((c_center.r-c_left.r)*(c_center.r-c_left.r) + (c_center.g-c_left.g)*(c_center.g-c_left.g) + (c_center.b-c_left.b)*(c_center.b-c_left.b));
			}else{
				left.x = left.y = left.z = sp_distance.ptr(v)[u].w = c_distance.ptr(v)[u].w = n_angles.ptr(v)[u].w = numeric_limits<float>::quiet_NaN ();
			}

			//right point
			if( u+1 < cols ){
				if( compute_distances )
				{
					right = make_float3( vmap.ptr (v)[u+1],vmap.ptr (v+rows)[u+1], vmap.ptr (v+2*rows)[u+1] );
					n_right = make_float3( nmap.ptr (v)[u+1],nmap.ptr (v+rows)[u+1], nmap.ptr (v+2*rows)[u+1] );
					if( isnan(right.x) || isnan(right.y) || isnan(right.z) ){
						sp_distance.ptr(v)[u].c1 = numeric_limits<float>::quiet_NaN ();
						n_angles.ptr(v)[u].c1 = numeric_limits<float>::quiet_NaN ();
					}else{
						sp_distance.ptr(v)[u].c1 = norm(right - center);
						n_angles.ptr(v)[u].c1 = dot(n_right,n_center)/(norm(n_right)*norm(n_center));
						n_angles.ptr(v)[u].c1 = acos( n_angles.ptr(v)[u].c1 );
						n_angles.ptr(v)[u].c1 = fabs( (n_angles.ptr(v)[u].c1 * 180) / 3.1416 ); 
					}
				}else
					right.x = right.y = right.z = sp_distance.ptr(v)[u].c1 = n_angles.ptr(v)[u].c1 = numeric_limits<float>::quiet_NaN ();

				c_right = cmap.ptr(v)[u+1];
				c_distance.ptr(v)[u].c1 = sqrtf((c_center.r-c_right.r)*(c_center.r-c_right.r) + (c_center.g-c_right.g)*(c_center.g-c_right.g) + (c_center.b-c_right.b)*(c_center.b-c_right.b));
			}else{
				right.x = right.y = right.z = sp_distance.ptr(v)[u].c1 = c_distance.ptr(v)[u].c1 = n_angles.ptr(v)[u].c1 = numeric_limits<float>::quiet_NaN ();
			}

			//leftdown point
			if( v+1 < rows && u-1 >= 0 ){
				if( compute_distances )
				{
					leftdown = make_float3( vmap.ptr (v+1)[u-1],vmap.ptr (v+rows+1)[u-1], vmap.ptr (v+2*rows+1)[u-1] );
					n_leftdown = make_float3( nmap.ptr (v+1)[u-1],nmap.ptr (v+rows+1)[u-1], nmap.ptr (v+2*rows+1)[u-1] );
					if( isnan(leftdown.x) || isnan(leftdown.y) || isnan(leftdown.z) ){
						sp_distance.ptr(v)[u].c2 = numeric_limits<float>::quiet_NaN ();
						n_angles.ptr(v)[u].c2 = numeric_limits<float>::quiet_NaN ();
					}else{
						sp_distance.ptr(v)[u].c2 = norm(leftdown - center);
						n_angles.ptr(v)[u].c2 = dot(n_leftdown,n_center)/(norm(n_leftdown)*norm(n_center));
						n_angles.ptr(v)[u].c2 = acos( n_angles.ptr(v)[u].c2 );
						n_angles.ptr(v)[u].c2 = fabs( (n_angles.ptr(v)[u].c2 * 180) / 3.1416 ); 
					}
				}else
					leftdown.x = leftdown.y = leftdown.z = sp_distance.ptr(v)[u].c2 = n_angles.ptr(v)[u].c2 = numeric_limits<float>::quiet_NaN ();

				c_leftdown = cmap.ptr(v+1)[v-1];
				c_distance.ptr(v)[u].c2 = sqrtf((c_center.r-c_leftdown.r)*(c_center.r-c_leftdown.r) + (c_center.g-c_leftdown.g)*(c_center.g-c_leftdown.g) + (c_center.b-c_leftdown.b)*(c_center.b-c_leftdown.b));
			}else{
				leftdown.x = leftdown.y = leftdown.z = sp_distance.ptr(v)[u].c2 = c_distance.ptr(v)[u].c2 = n_angles.ptr(v)[u].c2 = numeric_limits<float>::quiet_NaN ();
			}

			//down point
			if( v+1 < rows ){
				if( compute_distances )
				{
					down = make_float3( vmap.ptr (v+1)[u],vmap.ptr (v+rows+1)[u], vmap.ptr (v+2*rows+1)[u] );
					n_down = make_float3( nmap.ptr (v+1)[u],nmap.ptr (v+rows+1)[u], nmap.ptr (v+2*rows+1)[u] );
					if( isnan(down.x) || isnan(down.y) || isnan(down.z) ){
						sp_distance.ptr(v)[u].c3 = numeric_limits<float>::quiet_NaN ();
						n_angles.ptr(v)[u].c3 = numeric_limits<float>::quiet_NaN ();
					}else{
						sp_distance.ptr(v)[u].c3 = norm(down - center);
						n_angles.ptr(v)[u].c3 = dot(n_down,n_center)/(norm(n_down)*norm(n_center));
						n_angles.ptr(v)[u].c3 = acos( n_angles.ptr(v)[u].c3 );
						n_angles.ptr(v)[u].c3 = fabs( (n_angles.ptr(v)[u].c3 * 180) / 3.1416 ); 
					}
				}else
					down.x = down.y = down.z = sp_distance.ptr(v)[u].c3 = n_angles.ptr(v)[u].c3 = numeric_limits<float>::quiet_NaN ();	

				c_down = cmap.ptr(v+1)[v];
				c_distance.ptr(v)[u].c3 = sqrtf((c_center.r-c_down.r)*(c_center.r-c_down.r) + (c_center.g-c_down.g)*(c_center.g-c_down.g) + (c_center.b-c_down.b)*(c_center.b-c_down.b));
			}else{
				down.x = down.y = down.z = sp_distance.ptr(v)[u].c3 = c_distance.ptr(v)[u].c3 = n_angles.ptr(v)[u].c3 = numeric_limits<float>::quiet_NaN ();
			}

			//rightdown point
			if( v+1 < rows && u+1 < cols ){
				if( compute_distances )
				{
					rightdown = make_float3( vmap.ptr (v+1)[u+1],vmap.ptr (v+rows+1)[u+1], vmap.ptr (v+2*rows+1)[u+1] );
					n_rightdown = make_float3( nmap.ptr (v+1)[u+1],nmap.ptr (v+rows+1)[u+1], nmap.ptr (v+2*rows+1)[u+1] );
					if( isnan(rightdown.x) || isnan(rightdown.y) || isnan(rightdown.z) ){
						sp_distance.ptr(v)[u].c4 = numeric_limits<float>::quiet_NaN ();
						n_angles.ptr(v)[u].c4 = numeric_limits<float>::quiet_NaN ();
					}else{
						sp_distance.ptr(v)[u].c4 = norm(rightdown - center);
						n_angles.ptr(v)[u].c4 = fabs(dot(n_rightdown,n_center)/(norm(n_rightdown)*norm(n_center)));
						n_angles.ptr(v)[u].c4 = acos( n_angles.ptr(v)[u].c4 );
						n_angles.ptr(v)[u].c4 = fabs( (n_angles.ptr(v)[u].c4 * 180) / 3.1416 ); 
					}
				}else
					rightdown.x = rightdown.y = rightdown.z = sp_distance.ptr(v)[u].c4 = n_angles.ptr(v)[u].c4 = numeric_limits<float>::quiet_NaN ();

				c_rightdown = cmap.ptr(v+1)[u+1];
				c_distance.ptr(v)[u].c4 = sqrtf((c_center.r-c_rightdown.r)*(c_center.r-c_rightdown.r) + (c_center.g-c_rightdown.g)*(c_center.g-c_rightdown.g) + (c_center.b-c_rightdown.b)*(c_center.b-c_rightdown.b));
			}else{
				rightdown.x = rightdown.y = rightdown.z = sp_distance.ptr(v)[u].c4 = c_distance.ptr(v)[u].c4 = n_angles.ptr(v)[u].c4 = numeric_limits<float>::quiet_NaN ();
			}

			/*if( u==0 && v==0 )
			{
				printf(" c_distance.ptr(v)[v].x = %f \n",c_distance.ptr(v)[u].x);
				printf(" c_distance.ptr(v)[v].y = %f \n",c_distance.ptr(v)[u].y);
				printf(" c_distance.ptr(v)[v].z = %f \n",c_distance.ptr(v)[u].z);
				printf(" c_distance.ptr(v)[v].w = %f \n",c_distance.ptr(v)[u].w);
				printf(" c_distance.ptr(v)[v].c1 = %f \n",c_distance.ptr(v)[u].c1);
				printf(" c_distance.ptr(v)[v].c2 = %f \n",c_distance.ptr(v)[u].c2);
				printf(" c_distance.ptr(v)[v].c3 = %f \n",c_distance.ptr(v)[u].c3);
				printf(" c_distance.ptr(v)[v].c4 = %f \n",c_distance.ptr(v)[u].c4);
			}*/

			/*if( u==300 && v==250 )
			{
				printf(" sp_distance.ptr(v)[v].x = %f \n",sp_distance.ptr(v)[v].x);
				printf(" sp_distance.ptr(v)[v].y = %f \n",sp_distance.ptr(v)[v].y);
				printf(" sp_distance.ptr(v)[v].z = %f \n",sp_distance.ptr(v)[v].z);
				printf(" sp_distance.ptr(v)[v].w = %f \n",sp_distance.ptr(v)[v].w);
				printf(" sp_distance.ptr(v)[v].c1 = %f \n",sp_distance.ptr(v)[v].c1);
				printf(" sp_distance.ptr(v)[v].c2 = %f \n",sp_distance.ptr(v)[v].c2);
				printf(" sp_distance.ptr(v)[v].c3 = %f \n",sp_distance.ptr(v)[v].c3);
				printf(" sp_distance.ptr(v)[v].c4 = %f \n",sp_distance.ptr(v)[v].c4);
			}*/
		}

		__device__ float
		colorimetricalDistanceHSV(pcl::device::PixelHSV i, pcl::device::PixelHSV j)
		{
			float distance =	( i.sat*i.val*cosf(i.hue) - j.sat*j.val*cosf(j.hue)) * ( i.sat*i.val*cosf(i.hue) - j.sat*j.val*cosf(j.hue))
				+ ( i.sat*i.val*sinf(i.hue) - j.sat*j.val*sinf(j.hue)) * ( i.sat*i.val*sinf(i.hue) - j.sat*j.val*sinf(j.hue))
				+ ( i.val - j.val )*( i.val - j.val );
			return powf(distance,0.5);
		}

		__global__ void
    computeSegmentationGraphHSVKernel (int rows, int cols, const PtrStep<float> vmap, const PtrStep<pcl::device::PixelHSV> cmap, const PtrStep<float> nmap, 
					PtrStep<float8> sp_distance, PtrStep<float8> c_distance, PtrStep<float8> n_angles, PtrStep<float> mean_distances, PtrStep<float> mean_color, PtrStep<float> mean_angles)
    {
			int u = threadIdx.x + blockIdx.x * blockDim.x;
      int v = threadIdx.y + blockIdx.y * blockDim.y;

			bool compute_distances=true;
			bool compute_angles=true;

      if (u >= cols || v >= rows)
        return;

			float3 center, leftup, up, rightup, left, right, leftdown, down, rightdown;
			pcl::device::PixelHSV c_center, c_leftup, c_up, c_rightup, c_left, c_right, c_leftdown, c_down, c_rightdown;
			float3 n_center, n_leftup, n_up, n_rightup, n_left, n_right, n_leftdown, n_down, n_rightdown;

			//center point
			center = make_float3( vmap.ptr (v)[u],vmap.ptr (v+rows)[u], vmap.ptr (v+2*rows)[u] );
			n_center = make_float3( nmap.ptr (v)[u], nmap.ptr (v+rows)[u], nmap.ptr (v+2*rows)[u] );

			if( isnan(center.x) || isnan(center.y) || isnan(center.z) )
				compute_distances=false;

			if( isnan(n_center.x) || isnan(n_center.y) || isnan(n_center.z) )
				compute_angles=false;

			c_center = cmap.ptr(v)[u];
			
			//leftup point
			if( v-1 >= 0 && u-1 >= 0 ){
				if( compute_distances ){
					leftup = make_float3( vmap.ptr (v-1)[u-1],vmap.ptr (v+rows-1)[u-1], vmap.ptr (v+2*rows-1)[u-1] );
					if( compute_angles )
						n_leftup = make_float3( nmap.ptr (v-1)[u-1],nmap.ptr (v+rows-1)[u-1], nmap.ptr (v+2*rows-1)[u-1] );
					else
						n_angles.ptr(v)[u].x = numeric_limits<float>::quiet_NaN ();
					
					if( isnan(leftup.x) ){
						sp_distance.ptr(v)[u].x = numeric_limits<float>::quiet_NaN ();
						n_angles.ptr(v)[u].x = numeric_limits<float>::quiet_NaN ();
					}else{
						sp_distance.ptr(v)[u].x = norm(leftup - center);				
						if( compute_angles && !isnan(n_leftup.x) ){
							n_angles.ptr(v)[u].x = acos(dot(n_leftup,n_center));
							n_angles.ptr(v)[u].x = (n_angles.ptr(v)[u].x*180)/3.1416;
						}else
							n_angles.ptr(v)[u].x = numeric_limits<float>::quiet_NaN ();
					}
				}else
					leftup.x = leftup.y = leftup.z = sp_distance.ptr(v)[u].x = n_angles.ptr(v)[u].x = numeric_limits<float>::quiet_NaN ();

				c_leftup = cmap.ptr(v-1)[u-1];
				c_distance.ptr(v)[u].x = colorimetricalDistanceHSV( c_center,c_leftup );
			}else{
				leftup.x = leftup.y = leftup.z = sp_distance.ptr(v)[u].x = c_distance.ptr(v)[u].x = n_angles.ptr(v)[u].x = numeric_limits<float>::quiet_NaN ();
			}

			//up point
			if( v-1 >= 0 ){
				if( compute_distances ){
					up = make_float3( vmap.ptr (v-1)[u],vmap.ptr (v+rows-1)[u], vmap.ptr (v+2*rows-1)[u] );
					if( compute_angles )
						n_up = make_float3( nmap.ptr (v-1)[u],nmap.ptr (v+rows-1)[u], nmap.ptr (v+2*rows-1)[u] );
					else
						n_angles.ptr(v)[u].y = numeric_limits<float>::quiet_NaN ();
					
					if( isnan(up.x) ){
						sp_distance.ptr(v)[u].y = numeric_limits<float>::quiet_NaN ();
						n_angles.ptr(v)[u].y = numeric_limits<float>::quiet_NaN ();
					}else{
						sp_distance.ptr(v)[u].y = norm(up - center);				
						if( compute_angles && !isnan(n_up.x) ){
							n_angles.ptr(v)[u].y = acos(dot(n_up,n_center));
							n_angles.ptr(v)[u].y = (n_angles.ptr(v)[u].y*180)/3.1416;
						}else
							n_angles.ptr(v)[u].y = numeric_limits<float>::quiet_NaN ();
					}
				}else
					up.x = up.y = up.z = sp_distance.ptr(v)[u].y = n_angles.ptr(v)[u].y = numeric_limits<float>::quiet_NaN ();

				c_up = cmap.ptr(v-1)[u];
				c_distance.ptr(v)[u].y = colorimetricalDistanceHSV( c_center,c_up );
			}else{
				up.x = up.y = up.z = sp_distance.ptr(v)[u].y = c_distance.ptr(v)[u].y = n_angles.ptr(v)[u].y = numeric_limits<float>::quiet_NaN ();
			}

			//rightup point
			if( v-1 >= 0 && u+1 < cols ){
				if( compute_distances ){
					rightup = make_float3( vmap.ptr (v-1)[u+1],vmap.ptr (v+rows-1)[u+1], vmap.ptr (v+2*rows-1)[u+1] );
					if( compute_angles )
						n_rightup = make_float3( nmap.ptr (v-1)[u+1],nmap.ptr (v+rows-1)[u+1], nmap.ptr (v+2*rows-1)[u+1] );
					else
						n_angles.ptr(v)[u].z = numeric_limits<float>::quiet_NaN ();
					
					if( isnan(rightup.x) ){
						sp_distance.ptr(v)[u].z = numeric_limits<float>::quiet_NaN ();
						n_angles.ptr(v)[u].z = numeric_limits<float>::quiet_NaN ();
					}else{
						sp_distance.ptr(v)[u].z = norm(rightup - center);				
						if( compute_angles && !isnan(n_rightup.x) ){
							n_angles.ptr(v)[u].z = acos(dot(n_rightup,n_center));
							n_angles.ptr(v)[u].z = (n_angles.ptr(v)[u].z*180)/3.1416;
						}else
							n_angles.ptr(v)[u].z = numeric_limits<float>::quiet_NaN ();
					}
				}else
					rightup.x = rightup.y = rightup.z = sp_distance.ptr(v)[u].z = n_angles.ptr(v)[u].z = numeric_limits<float>::quiet_NaN ();

				c_rightup = cmap.ptr(v-1)[u+1];
				c_distance.ptr(v)[u].z = colorimetricalDistanceHSV( c_center,c_rightup );
			}else{
				rightup.x = rightup.y = rightup.z = sp_distance.ptr(v)[u].z = c_distance.ptr(v)[u].z = n_angles.ptr(v)[u].z = numeric_limits<float>::quiet_NaN ();
			}

			//left point
			if( u-1 >= 0 ){
				if( compute_distances ){
					left = make_float3( vmap.ptr (v)[u-1],vmap.ptr (v+rows)[u-1], vmap.ptr (v+2*rows)[u-1] );
					if( compute_angles )
						n_left = make_float3( nmap.ptr (v)[u-1],nmap.ptr (v+rows)[u-1], nmap.ptr (v+2*rows)[u-1] );
					else
						n_angles.ptr(v)[u].w = numeric_limits<float>::quiet_NaN ();
					
					if( isnan(left.x) ){
						sp_distance.ptr(v)[u].w = numeric_limits<float>::quiet_NaN ();
						n_angles.ptr(v)[u].w = numeric_limits<float>::quiet_NaN ();
					}else{
						sp_distance.ptr(v)[u].w = norm(left - center);				
						if( compute_angles && !isnan(n_left.x) ){
							n_angles.ptr(v)[u].w = acos(dot(n_left,n_center));
							n_angles.ptr(v)[u].w = (n_angles.ptr(v)[u].w*180)/3.1416;
						}else
							n_angles.ptr(v)[u].w = numeric_limits<float>::quiet_NaN ();
					}
				}else
					left.x = left.y = left.z = sp_distance.ptr(v)[u].w = n_angles.ptr(v)[u].w = numeric_limits<float>::quiet_NaN ();

				c_left = cmap.ptr(v)[u-1];
				c_distance.ptr(v)[u].w = colorimetricalDistanceHSV( c_center,c_left );
			}else{
				left.x = left.y = left.z = sp_distance.ptr(v)[u].w = c_distance.ptr(v)[u].w = n_angles.ptr(v)[u].w = numeric_limits<float>::quiet_NaN ();
			}

			//right point
			if( u+1 < cols ){
				if( compute_distances ){
					right = make_float3( vmap.ptr (v)[u+1],vmap.ptr (v+rows)[u+1], vmap.ptr (v+2*rows)[u+1] );
					if( compute_angles )
						n_right = make_float3( nmap.ptr (v)[u+1],nmap.ptr (v+rows)[u+1], nmap.ptr (v+2*rows)[u+1] );
					else
						n_angles.ptr(v)[u].c1 = numeric_limits<float>::quiet_NaN ();
					
					if( isnan(right.x) ){
						sp_distance.ptr(v)[u].c1 = numeric_limits<float>::quiet_NaN ();
						n_angles.ptr(v)[u].c1 = numeric_limits<float>::quiet_NaN ();
					}else{
						sp_distance.ptr(v)[u].c1 = norm(right - center);				
						if( compute_angles && !isnan(n_right.x) ){
							n_angles.ptr(v)[u].c1 = acos(dot(n_right,n_center));
							n_angles.ptr(v)[u].c1 = (n_angles.ptr(v)[u].c1*180)/3.1416;
						}else
							n_angles.ptr(v)[u].c1 = numeric_limits<float>::quiet_NaN ();
					}
				}else
					right.x = right.y = right.z = sp_distance.ptr(v)[u].c1 = n_angles.ptr(v)[u].c1 = numeric_limits<float>::quiet_NaN ();

				c_right = cmap.ptr(v)[u+1];
				c_distance.ptr(v)[u].c1 = colorimetricalDistanceHSV( c_center,c_right );
			}else{
				right.x = right.y = right.z = sp_distance.ptr(v)[u].c1 = c_distance.ptr(v)[u].c1 = n_angles.ptr(v)[u].c1 = numeric_limits<float>::quiet_NaN ();
			}

			//leftdown point
			if( v+1 < rows && u-1 >= 0 ){
				if( compute_distances ){
					leftdown = make_float3( vmap.ptr (v+1)[u-1],vmap.ptr (v+rows+1)[u-1], vmap.ptr (v+2*rows+1)[u-1] );
					if( compute_angles )
						n_leftdown = make_float3( nmap.ptr (v+1)[u-1],nmap.ptr (v+rows+1)[u-1], nmap.ptr (v+2*rows+1)[u-1] );
					else
						n_angles.ptr(v)[u].c2 = numeric_limits<float>::quiet_NaN ();
					
					if( isnan(leftdown.x) ){
						sp_distance.ptr(v)[u].c2 = numeric_limits<float>::quiet_NaN ();
						n_angles.ptr(v)[u].c2 = numeric_limits<float>::quiet_NaN ();
					}else{
						sp_distance.ptr(v)[u].c2 = norm(leftdown - center);				
						if( compute_angles && !isnan(n_leftdown.x) ){
							n_angles.ptr(v)[u].c2 = acos(dot(n_leftdown,n_center));
							n_angles.ptr(v)[u].c2 = (n_angles.ptr(v)[u].c2*180)/3.1416;
						}else
							n_angles.ptr(v)[u].c2 = numeric_limits<float>::quiet_NaN ();
					}
				}else
					leftdown.x = leftdown.y = leftdown.z = sp_distance.ptr(v)[u].c2 = n_angles.ptr(v)[u].c2 = numeric_limits<float>::quiet_NaN ();

				c_leftdown = cmap.ptr(v+1)[v-1];
				c_distance.ptr(v)[u].c2 = colorimetricalDistanceHSV( c_center,c_leftdown );
			}else{
				leftdown.x = leftdown.y = leftdown.z = sp_distance.ptr(v)[u].c2 = c_distance.ptr(v)[u].c2 = n_angles.ptr(v)[u].c2 = numeric_limits<float>::quiet_NaN ();
			}

			//down point
			if( v+1 < rows ){
				if( compute_distances ){
					down = make_float3( vmap.ptr (v+1)[u],vmap.ptr (v+rows+1)[u], vmap.ptr (v+2*rows+1)[u] );
					if( compute_angles )
						n_down = make_float3( nmap.ptr (v+1)[u],nmap.ptr (v+rows+1)[u], nmap.ptr (v+2*rows+1)[u] );
					else
						n_angles.ptr(v)[u].c3 = numeric_limits<float>::quiet_NaN ();
					
					if( isnan(down.x) ){
						sp_distance.ptr(v)[u].c3 = numeric_limits<float>::quiet_NaN ();
						n_angles.ptr(v)[u].c3 = numeric_limits<float>::quiet_NaN ();
					}else{
						sp_distance.ptr(v)[u].c3 = norm(down - center);				
						if( compute_angles && !isnan(n_down.x) ){
							n_angles.ptr(v)[u].c3 = acos(dot(n_down,n_center));
							n_angles.ptr(v)[u].c3 = (n_angles.ptr(v)[u].c3*180)/3.1416;
						}else
							n_angles.ptr(v)[u].c3 = numeric_limits<float>::quiet_NaN ();
					}
				}else
					down.x = down.y = down.z = sp_distance.ptr(v)[u].c3 = n_angles.ptr(v)[u].c3 = numeric_limits<float>::quiet_NaN ();

				c_down = cmap.ptr(v+1)[v];
				c_distance.ptr(v)[u].c3 = colorimetricalDistanceHSV( c_center,c_down );
			}else{
				down.x = down.y = down.z = sp_distance.ptr(v)[u].c3 = c_distance.ptr(v)[u].c3 = n_angles.ptr(v)[u].c3 = numeric_limits<float>::quiet_NaN ();
			}

			//rightdown point
			if( v+1 < rows && u+1 < cols ){
				if( compute_distances ){
					rightdown = make_float3( vmap.ptr (v+1)[u+1],vmap.ptr (v+rows+1)[u+1], vmap.ptr (v+2*rows+1)[u+1] );
					if( compute_angles )
						n_rightdown = make_float3( nmap.ptr (v+1)[u+1],nmap.ptr (v+rows+1)[u+1], nmap.ptr (v+2*rows+1)[u+1] );
					else
						n_angles.ptr(v)[u].c4 = numeric_limits<float>::quiet_NaN ();
					
					if( isnan(rightdown.x) ){
						sp_distance.ptr(v)[u].c4 = numeric_limits<float>::quiet_NaN ();
						n_angles.ptr(v)[u].c4 = numeric_limits<float>::quiet_NaN ();
					}else{
						sp_distance.ptr(v)[u].c4 = norm(rightdown - center);				
						if( compute_angles && !isnan(n_rightdown.x) ){
							n_angles.ptr(v)[u].c4 = acos(dot(n_rightdown,n_center));
							n_angles.ptr(v)[u].c4 = (n_angles.ptr(v)[u].c4*180)/3.1416;
						}else
							n_angles.ptr(v)[u].c4 = numeric_limits<float>::quiet_NaN ();
					}
				}else
					rightdown.x = rightdown.y = rightdown.z = sp_distance.ptr(v)[u].c4 = n_angles.ptr(v)[u].c4 = numeric_limits<float>::quiet_NaN ();

				c_rightdown = cmap.ptr(v+1)[u+1];
				c_distance.ptr(v)[u].c4 = colorimetricalDistanceHSV( c_center,c_rightdown );
			}else{
				rightdown.x = rightdown.y = rightdown.z = sp_distance.ptr(v)[u].c4 = c_distance.ptr(v)[u].c4 = n_angles.ptr(v)[u].c4 = numeric_limits<float>::quiet_NaN ();
			}

			// compute mean and median values for every pixel's neighborhood
			float aux=0.0;
			int counter=0;

			// mean 8-neighborhood color
			if( !isnan(c_distance.ptr(v)[u].x) ){	aux+=c_distance.ptr(v)[u].x; counter++; }
			if( !isnan(c_distance.ptr(v)[u].y) ){	aux+=c_distance.ptr(v)[u].y; counter++; }
			if( !isnan(c_distance.ptr(v)[u].z) ){	aux+=c_distance.ptr(v)[u].z; counter++; }
			if( !isnan(c_distance.ptr(v)[u].w) ){	aux+=c_distance.ptr(v)[u].w; counter++; }
			if( !isnan(c_distance.ptr(v)[u].c1) ){	aux+=c_distance.ptr(v)[u].c1; counter++; }
			if( !isnan(c_distance.ptr(v)[u].c2) ){	aux+=c_distance.ptr(v)[u].c2; counter++; }
			if( !isnan(c_distance.ptr(v)[u].c3) ){	aux+=c_distance.ptr(v)[u].c3; counter++; }
			if( !isnan(c_distance.ptr(v)[u].c4) ){	aux+=c_distance.ptr(v)[u].c4; counter++; }
			mean_color.ptr(v)[u] = aux / counter;

			// mean 8-neighborhood angles
			aux=0.0; counter=0;
			if( !isnan(n_angles.ptr(v)[u].x) ){	aux+=n_angles.ptr(v)[u].x; counter++; }
			if( !isnan(n_angles.ptr(v)[u].y) ){	aux+=n_angles.ptr(v)[u].y; counter++; }
			if( !isnan(n_angles.ptr(v)[u].z) ){	aux+=n_angles.ptr(v)[u].z; counter++; }
			if( !isnan(n_angles.ptr(v)[u].w) ){	aux+=n_angles.ptr(v)[u].w; counter++; }
			if( !isnan(n_angles.ptr(v)[u].c1) ){	aux+=n_angles.ptr(v)[u].c1; counter++; }
			if( !isnan(n_angles.ptr(v)[u].c2) ){	aux+=n_angles.ptr(v)[u].c2; counter++; }
			if( !isnan(n_angles.ptr(v)[u].c3) ){	aux+=n_angles.ptr(v)[u].c3; counter++; }
			if( !isnan(n_angles.ptr(v)[u].c4) ){	aux+=n_angles.ptr(v)[u].c4; counter++; }
			mean_angles.ptr(v)[u] = aux / counter;

			// mean 8-neighborhood distances
			aux=0.0; counter=0;
			if( !isnan(sp_distance.ptr(v)[u].x) ){	aux+=sp_distance.ptr(v)[u].x; counter++; }
			if( !isnan(sp_distance.ptr(v)[u].y) ){	aux+=sp_distance.ptr(v)[u].y; counter++; }
			if( !isnan(sp_distance.ptr(v)[u].z) ){	aux+=sp_distance.ptr(v)[u].z; counter++; }
			if( !isnan(sp_distance.ptr(v)[u].w) ){	aux+=sp_distance.ptr(v)[u].w; counter++; }
			if( !isnan(sp_distance.ptr(v)[u].c1) ){	aux+=sp_distance.ptr(v)[u].c1; counter++; }
			if( !isnan(sp_distance.ptr(v)[u].c2) ){	aux+=sp_distance.ptr(v)[u].c2; counter++; }
			if( !isnan(sp_distance.ptr(v)[u].c3) ){	aux+=sp_distance.ptr(v)[u].c3; counter++; }
			if( !isnan(sp_distance.ptr(v)[u].c4) ){	aux+=sp_distance.ptr(v)[u].c4; counter++; }
			mean_distances.ptr(v)[u] = aux / counter;

		}
		__global__ void
    computeOrganizedMeshKernel (int rows, int cols, const PtrStep<float> vmap, uint3* polygons, int triangle_pixel_size, float cos_angle_tolerance, float cloud_resolution)
    {
      int u = threadIdx.x + blockIdx.x * blockDim.x;
      int v = threadIdx.y + blockIdx.y * blockDim.y;
			
			int const uint_max = 400000;

			if (u < cols && v < rows)
			{
				// every thread could obtain up to 2 triangles. LEFT_CUT
				int global_index = (v*cols+u)*2;

				// no triangle found
				polygons[global_index].x=uint_max;
				polygons[global_index].y=uint_max;
				polygons[global_index].z=uint_max;

				polygons[global_index+1].x=uint_max;
				polygons[global_index+1].y=uint_max;
				polygons[global_index+1].z=uint_max;

				// check bounds
				if (u >= cols-triangle_pixel_size || v >= rows-triangle_pixel_size)
					return;

				float v_i_x = vmap.ptr (v)[u];
				float v_i_y = vmap.ptr (v +  rows)[u];
				float v_i_z = vmap.ptr (v +  2*rows)[u];
				unsigned int index_i = v*cols+u;

				float v_right_x = vmap.ptr (v)[u+triangle_pixel_size];
				float v_right_y = vmap.ptr (v +  rows)[u+triangle_pixel_size];
				float v_right_z = vmap.ptr (v +  2*rows)[u+triangle_pixel_size];
				unsigned int index_right = v*cols+u+1;
			
				float v_down_x = vmap.ptr (v + 1)[u];
				float v_down_y = vmap.ptr (v + rows + 1)[u];
				float v_down_z = vmap.ptr (v + 2*rows + 1)[u];
				unsigned int index_down = (v+1)*cols+u;

				float3 viewpoint = make_float3 (0.f, 0.f, 0.f);
				float3 dir_a = make_float3 (0.f, 0.f, 0.f);
				float3 dir_b = make_float3 (0.f, 0.f, 0.f);
				float3 dir_c = make_float3 (0.f, 0.f, 0.f);
				float cos_angle = 0.0f;
				float distance_to_points = 0.0f;
				float distance_between_points = 0.0f;

				if( isnan (v_down_x) || isnan (v_down_y) || isnan(v_down_z) )
					return;

				if( isnan (v_right_x) || isnan (v_right_y) || isnan(v_right_z) )
					return;	

				if( isnan (v_i_x) || isnan (v_i_y) || isnan (v_i_z) )
					goto TRIANGLE_2;

				// isShadowed(I,DOWN);
				dir_a = viewpoint - make_float3( v_i_x, v_i_y, v_i_z );
				dir_b = make_float3( v_down_x, v_down_y, v_down_z ) - make_float3( v_i_x, v_i_y, v_i_z );
				distance_to_points = norm (dir_a);
				distance_between_points = norm (dir_b);
				/*if( distance_between_points >= cloud_resolution*2)
					goto TRIANGLE_2;*/
				cos_angle = dot (dir_a,dir_b) / (distance_to_points*distance_between_points);
				if (fabs (cos_angle) >= cos_angle_tolerance)
					goto TRIANGLE_2;

				// isShadowed(DOWN,RIGHT);
				dir_b = viewpoint - make_float3( v_down_x, v_down_y, v_down_z );
				dir_c = make_float3( v_right_x, v_right_y, v_right_z ) - make_float3( v_down_x, v_down_y, v_down_z );
				distance_to_points = norm (dir_b);
				distance_between_points = norm (dir_c);
				/*if( distance_between_points >= cloud_resolution*2)
					goto TRIANGLE_2;*/
				cos_angle = dot (dir_b,dir_c) / (distance_to_points*distance_between_points);
				if (fabs (cos_angle) >= cos_angle_tolerance)
					goto TRIANGLE_2;

				// isShadowed(RIGHT,I);
				dir_c = viewpoint - make_float3( v_right_x, v_right_y, v_right_z );
				dir_a = make_float3( v_i_x, v_i_y, v_i_z ) - make_float3( v_right_x, v_right_y, v_right_z );
				distance_to_points = norm (dir_c);
				distance_between_points = norm (dir_a);
				/*if( distance_between_points >= cloud_resolution*2)
					goto TRIANGLE_2;*/
				cos_angle = dot (dir_c,dir_a) / (distance_to_points*distance_between_points);
				if (fabs (cos_angle) >= cos_angle_tolerance)
					goto TRIANGLE_2;

				polygons[global_index].x=index_i;
				polygons[global_index].y=index_down;
				polygons[global_index].z=index_right;

				TRIANGLE_2:
					float v_right_down_x = vmap.ptr (v + 1)[u+triangle_pixel_size];
					float v_right_down_y = vmap.ptr (v + rows + 1)[u+triangle_pixel_size];
					float v_right_down_z = vmap.ptr (v + 2*rows + 1)[u+triangle_pixel_size];
					unsigned int index_right_down = (v+1)*cols+u+1;

					if( isnan (v_right_down_x) || isnan(v_right_down_y) || isnan(v_right_down_z) )
						return;

					// isShadowed(RIGHT,DOWN);
					dir_a = viewpoint - make_float3( v_right_x, v_right_y, v_right_z );
					dir_b = make_float3( v_down_x, v_down_y, v_down_z ) - make_float3( v_right_x, v_right_y, v_right_z );
					distance_to_points = norm (dir_a);
					distance_between_points = norm (dir_b);
					/*if( distance_between_points >= cloud_resolution*2)
						return;*/
					cos_angle = dot (dir_a,dir_b) / (distance_to_points*distance_between_points);
					if (fabs (cos_angle) >= cos_angle_tolerance)
						return;

					dir_b = viewpoint - make_float3( v_down_x, v_down_y, v_down_z );
					dir_c = make_float3( v_right_down_x, v_right_down_y, v_right_down_z ) - make_float3( v_down_x, v_down_y, v_down_z );
					distance_to_points = norm (dir_b);
					distance_between_points = norm (dir_c);
					/*if( distance_between_points >= cloud_resolution*2)
						return;*/
					cos_angle = dot (dir_b,dir_c) / (distance_to_points*distance_between_points);
					if (fabs (cos_angle) >= cos_angle_tolerance)
						return;

					dir_c = viewpoint - make_float3( v_right_down_x, v_right_down_y, v_right_down_z );
					dir_a = make_float3( v_right_x, v_right_y, v_right_z ) - make_float3( v_right_down_x, v_right_down_y, v_right_down_z );
					distance_to_points = norm (dir_c);
					distance_between_points = norm (dir_a);
					/*if( distance_between_points >= cloud_resolution*2)
						return;*/
					cos_angle = dot (dir_c,dir_a) / (distance_to_points*distance_between_points);
					if (fabs (cos_angle) >= cos_angle_tolerance)
						return;
			
					polygons[global_index+1].x=index_right;
					polygons[global_index+1].y=index_down;
					polygons[global_index+1].z=index_right_down;

			}
		}

		enum
    {
      kx = 7,
      ky = 7,
      STEP = 1
    };

		__global__ void
    computeCloudResolutionKernel (int rows, int cols, const PtrStep<float> vmap, float *nearest_neighbor)
    {
			int u = threadIdx.x + blockIdx.x * blockDim.x;
      int v = threadIdx.y + blockIdx.y * blockDim.y;

			if (u >= cols || v >= rows)
        return;

			int global_index = (v*cols+u);

			float v_i_x = vmap.ptr (v)[u];
			float v_i_y = vmap.ptr (v +  rows)[u];
			float v_i_z = vmap.ptr (v +  2*rows)[u];
			float3 centroid = make_float3( v_i_x, v_i_y, v_i_z);

			int ty = min (v - ky / 2 + ky, rows - 1);
			int tx = min (u - kx / 2 + kx, cols - 1);

			float min_distance=numeric_limits<float>::max();
			for (int cy = max (v - ky / 2, 0); cy < ty; cy += STEP)
			{
				for (int cx = max (u - kx / 2, 0); cx < tx; cx += STEP)
				{
					float v_x = vmap.ptr (cy)[cx];
					if (!isnan (v_x) && cy != v && cx != u)
					{
						float v_y = vmap.ptr (cy + rows)[cx];
						float v_z = vmap.ptr (cy + 2 * rows)[cx];
						float3 v = make_float3(v_x,v_y,v_z) - centroid;
						float aux = norm(v);
						if( aux < min_distance )
							min_distance = aux;
					}
				}
			}

			nearest_neighbor[global_index]=min_distance;
		}

		// useful macros for hsv conversion
		//
		#define MIN3(x,y,z)  ((y) <= (z) ? \
                         ((x) <= (y) ? (x) : (y)) \
                     : \
                         ((x) <= (z) ? (x) : (z)))

		#define MAX3(x,y,z)  ((y) >= (z) ? \
														 ((x) >= (y) ? (x) : (y)) \
												 : \
														 ((x) >= (z) ? (x) : (z)))

		__global__ void
		convertRGBtoGrayKernel (int rows, int cols, const PtrStep<pcl::gpu::PixelRGB> cmap, PtrStep<float> gray_map, const int conversion_type)
		{
			int u = threadIdx.x + blockIdx.x * blockDim.x;
			int v = threadIdx.y + blockIdx.y * blockDim.y;

			if (u < cols && v < rows)
			{
				pcl::gpu::PixelRGB rgb = cmap.ptr(v)[u];
				
				// average
				if( conversion_type == 0)
				{
					gray_map.ptr(v)[u] = (rgb.r + rgb.g + rgb.b)/3;
				}
				// lighting
				else if( conversion_type == 1)
				{
					gray_map.ptr(v)[u] = (MAX3(rgb.r, rgb.g, rgb.b) + MIN3(rgb.r, rgb.g, rgb.b))/2;
				}
				// luminosity
				else
				{
					gray_map.ptr(v)[u] = (0.21f * rgb.r + 0.71f * rgb.g + 0.07f * rgb.b);
				}
				gray_map.ptr(v)[u] /= 255;
			}
		}

		__global__ void
    convertRGBtoHSVKernel (int rows, int cols, const PtrStep<pcl::gpu::PixelRGB> cmap, PtrStep<pcl::device::PixelHSV> hsv_cmap)
    {
			int u = threadIdx.x + blockIdx.x * blockDim.x;
			int v = threadIdx.y + blockIdx.y * blockDim.y;

			if (u < cols && v < rows)
			{
				pcl::gpu::PixelRGB rgb = cmap.ptr(v)[u];
				pcl::device::PixelHSV hsv;

				float r,g,b,rgb_min,rgb_max;
				r = (float)(rgb.r);
				g = (float)(rgb.g);
				b = (float)(rgb.b);

				r/=255.f;
				g/=255.f;
				b/=255.f;

				rgb_min = MIN3(r, g, b);
				rgb_max = MAX3(r, g, b);
				float chroma = rgb_max - rgb_min;
 
				//If Chroma is 0, then S is 0 by definition, and H is undefined but 0 by convention.
				if(chroma != 0)
				{
					if(r == rgb_max)
					{
						hsv.hue = (g - b) / chroma;
 
						if(hsv.hue < 0.0)
						{
							hsv.hue += 6.0;
						}
					}
					else if(g == rgb_max)
					{
						hsv.hue = ((b - r) / chroma) + 2.0;
					}
					else //RGB.B == Max
					{
						hsv.hue = ((r - g) / chroma) + 4.0;
					}
 
					hsv.hue *= 60.0;
					hsv.sat = chroma / rgb_max;
				}
 
				hsv.hue = hsv.sat = 0;
				hsv.val = rgb_max;
				hsv_cmap.ptr(v)[u] = hsv;
			}	
		}

		__global__ void
		computeDistancesMapKernel (const PtrStep<float> vmap, const float3 point, PtrStep<float> distances_map, PtrStep<float3> normalized_directions, int rows, int cols)
		{
			int u = threadIdx.x + blockIdx.x * blockDim.x;
			int v = threadIdx.y + blockIdx.y * blockDim.y;

			if (u < cols && v < rows)
			{
				float bad_point = numeric_limits<float>::quiet_NaN ();

				float v_i_x = vmap.ptr (v)[u];
				float v_i_y = vmap.ptr (v +  rows)[u];
				float v_i_z = vmap.ptr (v +  2*rows)[u];

				if( isnan(v_i_x) || isnan(v_i_y) || isnan(v_i_z) )
				{
					distances_map.ptr(v)[u] = bad_point;
					normalized_directions.ptr(v)[u] = make_float3(bad_point,bad_point,bad_point);
					return;
				}

				float3 p = make_float3( v_i_x, v_i_y, v_i_z);
				float3 direction = point-p;
			
				distances_map.ptr(v)[u] = norm(direction);
				normalized_directions.ptr(v)[u] = normalized(direction);
			}
		}

		__global__ void
		computeAttenuationReflectanceAlbedoMapsKernel(const PtrStep<float> dmap_pointLight,const PtrStep<float> dmap_pointView, PtrStep<float> attenuation_map, const PtrStep<float> nmap, 
			const PtrStep<float3> directions_pointLight, PtrStep<float> reflectance_map, const float attenuationFactor, const PtrStep<float> grayscaleImage, PtrStep<float> albedo_map, 
			const int rows, const int cols)
		{
			int u = threadIdx.x + blockIdx.x * blockDim.x;
			int v = threadIdx.y + blockIdx.y * blockDim.y;

			if (u < cols && v < rows)
			{
				float bad_point = numeric_limits<float>::quiet_NaN ();

				float dv_pointLight = dmap_pointLight.ptr (v)[u];
				float dv_pointView = dmap_pointView.ptr (v)[u];

				if( isnan(dv_pointLight) || isnan(dv_pointView) )
				{
					attenuation_map.ptr(v)[u] = bad_point;
					reflectance_map.ptr(v)[u] = bad_point;
					albedo_map.ptr(v)[u] = bad_point;
					return;
				}
				
				// Att = exp((-k.*DistToViewer)./(DistToLight.*DistToLight));
				attenuation_map.ptr(v)[u] = exp((-attenuationFactor*dv_pointView)/(dv_pointLight*dv_pointLight));
			
				// Reflectance calculus
				float n_x = nmap.ptr (v)[u];
				float n_y = nmap.ptr (v +  rows)[u];
				float n_z = nmap.ptr (v +  2*rows)[u];
				float3 normal_p = make_float3(n_x,n_y,n_z);

				if( isnan(n_x) || isnan(n_y) || isnan(n_z) )
				{
					reflectance_map.ptr(v)[u] = bad_point;
					albedo_map.ptr(v)[u] = bad_point;
					return;
				}

				float3 direction_p = directions_pointLight.ptr(v)[u];
				if( isnan(direction_p.x) || isnan(direction_p.y) || isnan(direction_p.z) )
				{
					reflectance_map.ptr(v)[u] = bad_point;
					albedo_map.ptr(v)[u] = bad_point;
					return;
				}

				float reflectance = attenuation_map.ptr(v)[u] * dot(normal_p,direction_p);
				// remove shadows: reflectance < 0
				if( reflectance < 0 ){
					reflectance_map.ptr(v)[u] = 0;
					albedo_map.ptr(v)[u] = 0;
				}else
				{
					reflectance_map.ptr(v)[u] = reflectance;
					//calculate albedo factor
					albedo_map.ptr(v)[u] = grayscaleImage.ptr(v)[u] / reflectance; 
				}
			}
		}
	}
}

void
pcl::device::computeSegmentationGraph(const MapArr& vmap, const View& cmap, const MapArr& nmap, 
	DeviceArray2D<float8>& sp_distances, DeviceArray2D<float8>& c_distances, DeviceArray2D<float8>& n_angles, int rows,int cols)
{
	sp_distances.create(rows,cols);
	c_distances.create(rows,cols);
	n_angles.create(rows,cols);
	dim3 block(32,8);
	dim3 grid(1,1,1);
	grid.x = divUp (cols, block.x);
	grid.y = divUp (rows, block.y);

	computeSegmentationGraphKernel<<<grid,block>>>(rows,cols,vmap,cmap,nmap,sp_distances,c_distances,n_angles);

	cudaSafeCall (cudaGetLastError ());
}

void
	pcl::device::computeSegmentationGraphHSV(const MapArr& vmap, const DeviceArray2D<pcl::device::PixelHSV>& cmap, const MapArr& nmap, 
	DeviceArray2D<float8>& sp_distances, DeviceArray2D<float8>& c_distances, DeviceArray2D<float8>& n_angles, DeviceArray2D<float> &mean_distances, DeviceArray2D<float> &mean_color, DeviceArray2D<float> &mean_angles, int rows, int cols)
{
	sp_distances.create(rows,cols);
	c_distances.create(rows,cols);
	n_angles.create(rows,cols);

	mean_distances.create(rows,cols);
	mean_color.create(rows,cols);
	mean_angles.create(rows,cols);

	dim3 block(32,8);
	dim3 grid(1,1,1);
	grid.x = divUp (cols, block.x);
	grid.y = divUp (rows, block.y);

	computeSegmentationGraphHSVKernel<<<grid,block>>>(rows,cols,vmap,cmap,nmap,sp_distances,c_distances,n_angles, mean_distances, mean_color, mean_angles);

	cudaSafeCall (cudaGetLastError ());
}

void
pcl::device::reconstructOFM(const MapArr& vmap, DeviceArray<uint3>& polygons,int rows,int cols,float cloud_resolution)
{
	int triangle_pixel_size = 1;
	float cos_angle_tolerance = fabsf (cosf (0.017453293f * 12.5f));

	polygons.create(2*rows*cols);
	dim3 block(32,8);
	dim3 grid(1,1,1);
	grid.x = divUp (cols, block.x);
  grid.y = divUp (rows, block.y);

	computeOrganizedMeshKernel<<<grid,block>>>(rows,cols,vmap,polygons.ptr(),triangle_pixel_size,cos_angle_tolerance,cloud_resolution);

	cudaSafeCall (cudaGetLastError ());
}

float
pcl::device::computeCloudResolution(const MapArr& vmap, int rows,int cols)
{
	dim3 block(32,8);
	dim3 grid(1,1,1);
	grid.x = divUp (cols, block.x);
  grid.y = divUp (rows, block.y);

	DeviceArray<float> nearest_neighbors;
	nearest_neighbors.create(rows*cols);
	computeCloudResolutionKernel<<<grid,block>>>(rows,cols,vmap,nearest_neighbors.ptr());
	cudaSafeCall (cudaGetLastError ());

	std::vector<float> min_distances;
	nearest_neighbors.download(min_distances);
	
	float mean=0.0f;
	int counter=0;
	for(int i=0;i<min_distances.size();i++)
	{
		if( min_distances[i] != std::numeric_limits<float>::max() )
		{
			mean+=min_distances[i];
			counter++;
		}
	}
  return mean/counter;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void
pcl::device::computeDistancesMap (const MapArr& vmap,const float3 point, DeviceArray2D<float> &dmap, DeviceArray2D<float3> &directions_map, int rows, int cols)
{
  dmap.create (rows,cols);
	directions_map.create (rows,cols);

  dim3 block (32, 8);
  dim3 grid (1, 1, 1);
  grid.x = divUp (cols, block.x);
  grid.y = divUp (rows, block.y);

  computeDistancesMapKernel<<<grid, block>>>(vmap, point, dmap,directions_map, rows, cols);
  cudaSafeCall (cudaGetLastError ());
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void
pcl::device::convertRGBtoGray (const int rows,const int cols,const View& cmap, DeviceArray2D<float>& gray_map, const int conversion_type)
{
  gray_map.create (rows,cols);

  dim3 block (32, 8);
  dim3 grid (1, 1, 1);
  grid.x = divUp (cols, block.x);
  grid.y = divUp (rows, block.y);

  convertRGBtoGrayKernel<<<grid, block>>>(rows, cols, cmap, gray_map, conversion_type);
  cudaSafeCall (cudaGetLastError ());
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void
pcl::device::convertRGBtoHSV (const int rows,const int cols,const View& cmap, DeviceArray2D<pcl::device::PixelHSV>& hsv_map)
{
  hsv_map.create (rows,cols);

  dim3 block (32, 8);
  dim3 grid (1, 1, 1);
  grid.x = divUp (cols, block.x);
  grid.y = divUp (rows, block.y);

  convertRGBtoHSVKernel<<<grid, block>>>(rows, cols, cmap, hsv_map);
  cudaSafeCall (cudaGetLastError ());
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void
pcl::device::computeAttenuationReflectanceMaps (const DeviceArray2D<float> &dmap_pointLight,const DeviceArray2D<float> &dmap_pointView, DeviceArray2D<float> &attenuation_map,
	const DeviceArray2D<float> nmap, const DeviceArray2D<float3> directions_pointLight, DeviceArray2D<float> &reflectance_map, const float attenuationFactor,
	const DeviceArray2D<float> grayscale_image, DeviceArray2D<float> &albedo_map, int rows, int cols)
{
  attenuation_map.create (rows,cols);
	reflectance_map.create (rows,cols);
	albedo_map.create(rows, cols);

  dim3 block (32, 8);
  dim3 grid (1, 1, 1);
  grid.x = divUp (cols, block.x);
  grid.y = divUp (rows, block.y);

  computeAttenuationReflectanceAlbedoMapsKernel<<<grid, block>>>(dmap_pointLight, dmap_pointView, attenuation_map, nmap, directions_pointLight, reflectance_map, attenuationFactor, grayscale_image, albedo_map, rows, cols);
  cudaSafeCall (cudaGetLastError ());
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void
pcl::device::createVMap (const float fx_, const float fy_, const DepthMap& depth, MapArr& vmap)
{
  vmap.create (depth.rows () * 3, depth.cols ());

  dim3 block (32, 8);
  dim3 grid (1, 1, 1);
  grid.x = divUp (depth.cols (), block.x);
  grid.y = divUp (depth.rows (), block.y);

  float fx = fx_, cx = depth.cols() >> 1;
  float fy = fy_, cy = depth.rows() >> 1;

  computeVmapKernel<<<grid, block>>>(depth, vmap, fx, fy, cx, cy);
  cudaSafeCall (cudaGetLastError ());
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void
pcl::device::createNMap (const MapArr& vmap, MapArr& nmap)
{
  nmap.create (vmap.rows (), vmap.cols ());

  int rows = vmap.rows () / 3;
  int cols = vmap.cols ();

  dim3 block (32, 8);
  dim3 grid (1, 1, 1);
  grid.x = divUp (cols, block.x);
  grid.y = divUp (rows, block.y);

  computeNmapKernel<<<grid, block>>>(rows, cols, vmap, nmap);
  cudaSafeCall (cudaGetLastError ());
}

namespace pcl
{
  namespace device
  {
    __global__ void
    tranformMapsKernel (int rows, int cols, const PtrStep<float> vmap_src, const PtrStep<float> nmap_src,
                        const Mat33 Rmat, const float3 tvec, PtrStepSz<float> vmap_dst, PtrStep<float> nmap_dst)
    {
      int x = threadIdx.x + blockIdx.x * blockDim.x;
      int y = threadIdx.y + blockIdx.y * blockDim.y;

      const float qnan = pcl::device::numeric_limits<float>::quiet_NaN ();

      if (x < cols && y < rows)
      {
        //vetexes
        float3 vsrc, vdst = make_float3 (qnan, qnan, qnan);
        vsrc.x = vmap_src.ptr (y)[x];

        if (!isnan (vsrc.x))
        {
          vsrc.y = vmap_src.ptr (y + rows)[x];
          vsrc.z = vmap_src.ptr (y + 2 * rows)[x];

          vdst = Rmat * vsrc + tvec;

          vmap_dst.ptr (y + rows)[x] = vdst.y;
          vmap_dst.ptr (y + 2 * rows)[x] = vdst.z;
        }

        vmap_dst.ptr (y)[x] = vdst.x;

        //normals
        float3 nsrc, ndst = make_float3 (qnan, qnan, qnan);
        nsrc.x = nmap_src.ptr (y)[x];

        if (!isnan (nsrc.x))
        {
          nsrc.y = nmap_src.ptr (y + rows)[x];
          nsrc.z = nmap_src.ptr (y + 2 * rows)[x];

          ndst = Rmat * nsrc;

          nmap_dst.ptr (y + rows)[x] = ndst.y;
          nmap_dst.ptr (y + 2 * rows)[x] = ndst.z;
        }

        nmap_dst.ptr (y)[x] = ndst.x;
      }
    }
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void
pcl::device::tranformMaps (const MapArr& vmap_src, const MapArr& nmap_src,
                           const Mat33& Rmat, const float3& tvec,
                           MapArr& vmap_dst, MapArr& nmap_dst)
{
  int cols = vmap_src.cols ();
  int rows = vmap_src.rows () / 3;

  vmap_dst.create (rows * 3, cols);
  nmap_dst.create (rows * 3, cols);

  dim3 block (32, 8);
  dim3 grid (1, 1, 1);
  grid.x = divUp (cols, block.x);
  grid.y = divUp (rows, block.y);

  tranformMapsKernel<<<grid, block>>>(rows, cols, vmap_src, nmap_src, Rmat, tvec, vmap_dst, nmap_dst);
  cudaSafeCall (cudaGetLastError ());

  cudaSafeCall (cudaDeviceSynchronize ());
}

namespace pcl
{
  namespace device
  {
    template<bool normalize>
    __global__ void
    resizeMapKernel (int drows, int dcols, int srows, const PtrStep<float> input, PtrStep<float> output)
    {
      int x = threadIdx.x + blockIdx.x * blockDim.x;
      int y = threadIdx.y + blockIdx.y * blockDim.y;

      if (x >= dcols || y >= drows)
        return;

      const float qnan = numeric_limits<float>::quiet_NaN ();

      int xs = x * 2;
      int ys = y * 2;

      float x00 = input.ptr (ys + 0)[xs + 0];
      float x01 = input.ptr (ys + 0)[xs + 1];
      float x10 = input.ptr (ys + 1)[xs + 0];
      float x11 = input.ptr (ys + 1)[xs + 1];

      if (isnan (x00) || isnan (x01) || isnan (x10) || isnan (x11))
      {
        output.ptr (y)[x] = qnan;
        return;
      }
      else
      {
        float3 n;

        n.x = (x00 + x01 + x10 + x11) / 4;

        float y00 = input.ptr (ys + srows + 0)[xs + 0];
        float y01 = input.ptr (ys + srows + 0)[xs + 1];
        float y10 = input.ptr (ys + srows + 1)[xs + 0];
        float y11 = input.ptr (ys + srows + 1)[xs + 1];

        n.y = (y00 + y01 + y10 + y11) / 4;

        float z00 = input.ptr (ys + 2 * srows + 0)[xs + 0];
        float z01 = input.ptr (ys + 2 * srows + 0)[xs + 1];
        float z10 = input.ptr (ys + 2 * srows + 1)[xs + 0];
        float z11 = input.ptr (ys + 2 * srows + 1)[xs + 1];

        n.z = (z00 + z01 + z10 + z11) / 4;

        if (normalize)
          n = normalized (n);

        output.ptr (y        )[x] = n.x;
        output.ptr (y + drows)[x] = n.y;
        output.ptr (y + 2 * drows)[x] = n.z;
      }
    }

    template<bool normalize>
    void
    resizeMap (const MapArr& input, MapArr& output)
    {
      int in_cols = input.cols ();
      int in_rows = input.rows () / 3;

      int out_cols = in_cols / 2;
      int out_rows = in_rows / 2;

      output.create (out_rows * 3, out_cols);

      dim3 block (32, 8);
      dim3 grid (divUp (out_cols, block.x), divUp (out_rows, block.y));
      resizeMapKernel<normalize><< < grid, block>>>(out_rows, out_cols, in_rows, input, output);
      cudaSafeCall ( cudaGetLastError () );
      cudaSafeCall (cudaDeviceSynchronize ());
    }
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void
pcl::device::resizeVMap (const MapArr& input, MapArr& output)
{
  resizeMap<false>(input, output);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void
pcl::device::resizeNMap (const MapArr& input, MapArr& output)
{
  resizeMap<true>(input, output);
}

namespace pcl
{
  namespace device
  {

    template<typename T>
    __global__ void
    convertMapKernel (int rows, int cols, const PtrStep<float> map, PtrStep<T> output)
    {
      int x = threadIdx.x + blockIdx.x * blockDim.x;
      int y = threadIdx.y + blockIdx.y * blockDim.y;

      if (x >= cols || y >= rows)
        return;

      const float qnan = numeric_limits<float>::quiet_NaN ();

      T t;
      t.x = map.ptr (y)[x];
      if (!isnan (t.x))
      {
        t.y = map.ptr (y + rows)[x];
        t.z = map.ptr (y + 2 * rows)[x];
      }
      else
        t.y = t.z = qnan;

      output.ptr (y)[x] = t;
    }
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<typename T> void
pcl::device::convert (const MapArr& vmap, DeviceArray2D<T>& output)
{
  int cols = vmap.cols ();
  int rows = vmap.rows () / 3;

  output.create (rows, cols);

  dim3 block (32, 8);
  dim3 grid (divUp (cols, block.x), divUp (rows, block.y));

  convertMapKernel<T><< < grid, block>>>(rows, cols, vmap, output);
  cudaSafeCall ( cudaGetLastError () );
  cudaSafeCall (cudaDeviceSynchronize ());
}

template void pcl::device::convert (const MapArr& vmap, DeviceArray2D<float4>& output);
template void pcl::device::convert (const MapArr& vmap, DeviceArray2D<float8>& output);

namespace pcl
{
  namespace device
  {
    __global__ void
    mergePointNormalKernel (const float4* cloud, const float8* normals, PtrSz<float12> output)
    {
      int idx = threadIdx.x + blockIdx.x * blockDim.x;

      if (idx < output.size)
      {
        float4 p = cloud[idx];
        float8 n = normals[idx];

        float12 o;
        o.x = p.x;
        o.y = p.y;
        o.z = p.z;

        o.normal_x = n.x;
        o.normal_y = n.y;
        o.normal_z = n.z;

        output.data[idx] = o;
      }
    }
  }
}

void
pcl::device::mergePointNormal (const DeviceArray<float4>& cloud, const DeviceArray<float8>& normals, const DeviceArray<float12>& output)
{
  const int block = 256;
  int total = (int)output.size ();

  mergePointNormalKernel<<<divUp (total, block), block>>>(cloud, normals, output);
  cudaSafeCall ( cudaGetLastError () );
  cudaSafeCall (cudaDeviceSynchronize ());
}
