/*
 * Software License Agreement (BSD License)
 *
 *  Point Cloud Library (PCL) - www.pointclouds.org
 *  Copyright (c) 2011, Willow Garage, Inc.
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "device.hpp"
//#include <pcl/gpu/features/device/eigen.hpp>

namespace pcl
{
  namespace device
  {

		enum
    {
      dx = 15,
      dy = 15,
      STEP = 1
    };

		__global__ void
		computeCovariancesMapKernel(int rows, int cols, const PtrStep<float> vmap, const PtrStep<PixelHSV> cmap, PtrStep<pcl::device::symMat66> covmap)
		{
			int u = threadIdx.x + blockIdx.x * blockDim.x;
      int v = threadIdx.y + blockIdx.y * blockDim.y;

			if (u >= cols || v >= rows)
        return;

			// default values 
			covmap.ptr(v)[u].x00 = numeric_limits<float>::quiet_NaN ();

      if (isnan (vmap.ptr (v)[u]) )
        return;

      int ty = min (v - dy / 2 + dy, rows - 1);
      int tx = min (u - dx / 2 + dx, cols - 1);

			// calculate mean
      float6 centroid = make_float6 (0.f, 0.f, 0.f, 0.f, 0.f, 0.f);

      int counter = 0;
      for (int cy = max (v - dy / 2, 0); cy < ty; cy += STEP)
        for (int cx = max (u - dx / 2, 0); cx < tx; cx += STEP)
        {
          float v_x = vmap.ptr (cy)[cx];
          if (!isnan (v_x))
          {
            centroid.x += v_x;
            centroid.y += vmap.ptr (cy + rows)[cx];
            centroid.z += vmap.ptr (cy + 2 * rows)[cx];

						centroid.c1 += cmap.ptr(cy)[cx].sat*cmap.ptr(cy)[cx].val*cosf(cmap.ptr(cy)[cx].hue); // sat*val*cos(hue)
						centroid.c2 += cmap.ptr(cy)[cx].sat*cmap.ptr(cy)[cx].val*sinf(cmap.ptr(cy)[cx].hue); // sat*val*sin(hue)
						centroid.c3 += cmap.ptr(cy)[cx].val;																								 // val
            ++counter;
          }
        }

				centroid *= 1.f / counter;

			// calculate covariance matrix
      float cov[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

      for (int cy = max (v - dy / 2, 0); cy < ty; cy += STEP)
        for (int cx = max (u - dx / 2, 0); cx < tx; cx += STEP)
        {
          float6 v;
          v.x = vmap.ptr (cy)[cx];
          if (isnan (v.x))
            continue;

          v.y = vmap.ptr (cy + rows)[cx];
          v.z = vmap.ptr (cy + 2 * rows)[cx];

					v.c1 += cmap.ptr(cy)[cx].sat*cmap.ptr(cy)[cx].val*cosf(cmap.ptr(cy)[cx].hue); // sat*val*cos(hue)
					v.c2 += cmap.ptr(cy)[cx].sat*cmap.ptr(cy)[cx].val*sinf(cmap.ptr(cy)[cx].hue); // sat*val*sin(hue)
					v.c3 += cmap.ptr(cy)[cx].val;	

          float6 d = v - centroid;

          cov[0] += d.x * d.x;               //cov (0, 0)
          cov[1] += d.x * d.y;               //cov (0, 1)
          cov[2] += d.x * d.z;               //cov (0, 2)
					cov[3] += d.x * d.c1;							 //cov (0, 3)
					cov[4] += d.x * d.c2;							 //cov (0, 4)
					cov[5] += d.x * d.c3;							 //cov (0, 5)

          cov[6] += d.y * d.y;               //cov (1, 1)
          cov[7] += d.y * d.z;               //cov (1, 2)
					cov[8] += d.y * d.c1;							 //cov (1, 3)
					cov[9] += d.y * d.c2;							 //cov (1, 4)
					cov[10] += d.y * d.c3;						 //cov (1, 5)
          
					cov[11] += d.z * d.z;               //cov (2, 2)
					cov[12] += d.z * d.c1;							//cov (2 ,3)
					cov[13] += d.z * d.c2;							//cov (2 ,4)
					cov[14] += d.z * d.c3;							//cov (2 ,5)

					cov[15] += d.c1 * d.c1;             //cov (3, 3)
					cov[16] += d.c1 * d.c2;							//cov (3 ,4)
					cov[17] += d.c1 * d.c3;							//cov (3 ,5)
					cov[18] += d.c2 * d.c2;							//cov (4 ,4)
					cov[19] += d.c2 * d.c3;							//cov (4 ,5)
					cov[20] += d.c3 * d.c3;							//cov (5 ,5)
				}

				covmap.ptr(v)[u].x00 = cov[0];
				covmap.ptr(v)[u].x01 = cov[1];     //cov (0, 1)
        covmap.ptr(v)[u].x02 = cov[2];     //cov (0, 2)
				covmap.ptr(v)[u].x03 = cov[3];     //cov (0, 3)
				covmap.ptr(v)[u].x04 = cov[4];     //cov (0, 2)
				covmap.ptr(v)[u].x05 = cov[5];     //cov (0, 2)

        covmap.ptr(v)[u].x11 = cov[6];     //cov (1, 1)
				covmap.ptr(v)[u].x12 = cov[7];     //cov (1, 2)
				covmap.ptr(v)[u].x13 = cov[8];     //cov (1, 3)
				covmap.ptr(v)[u].x14 = cov[9];     //cov (1, 4)
				covmap.ptr(v)[u].x15 = cov[10];    //cov (1, 5)

				covmap.ptr(v)[u].x22 = cov[11];     //cov (2, 2)
				covmap.ptr(v)[u].x23 = cov[12];     //cov (2, 3)
				covmap.ptr(v)[u].x24 = cov[13];     //cov (2, 4)
				covmap.ptr(v)[u].x25 = cov[14];     //cov (2, 5)

				covmap.ptr(v)[u].x33 = cov[15];     //cov (3, 3)
				covmap.ptr(v)[u].x34 = cov[16];     //cov (3, 4)
				covmap.ptr(v)[u].x35 = cov[17];     //cov (3, 5)
				covmap.ptr(v)[u].x44 = cov[18];     //cov (4, 4)
				covmap.ptr(v)[u].x45 = cov[19];     //cov (4, 5)
				covmap.ptr(v)[u].x55 = cov[20];     //cov (5, 5)

      /*typedef Eigen33::Mat33 Mat33;
      Eigen33 eigen33 (cov);

      Mat33 tmp;
      Mat33 vec_tmp;
      Mat33 evecs;
      float3 evals;
      eigen33.compute (tmp, vec_tmp, evecs, evals);

      float3 n = normalized (evecs[0]);

      u = threadIdx.x + blockIdx.x * blockDim.x;
      v = threadIdx.y + blockIdx.y * blockDim.y;

			nmap.ptr (v       )[u] = n.x;
			nmap.ptr (v + rows)[u] = n.y;
			nmap.ptr (v + 2 * rows)[u] = n.z;*/
		}
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void
pcl::device::computeCovariancesMap (const DeviceArray2D<float>& vmap, const DeviceArray2D<pcl::device::PixelHSV>& cmap, DeviceArray2D<pcl::device::symMat66>& covmap)
{
  int cols = vmap.cols ();
  int rows = vmap.rows () / 3; 

  covmap.create (rows, cols);

  dim3 block (32, 8);
  dim3 grid (1, 1, 1);
  grid.x = divUp (cols, block.x);
  grid.y = divUp (rows, block.y);

  computeCovariancesMapKernel<<<grid, block>>>(rows, cols, vmap, cmap, covmap);
  cudaSafeCall (cudaGetLastError ());
  cudaSafeCall (cudaDeviceSynchronize ());
}

