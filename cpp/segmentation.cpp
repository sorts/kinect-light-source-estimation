#include <algorithm>
#include <iostream>
#include <vector>
#include <stack>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/pcl_base.h>
#include <pcl/pcl_macros.h>
#include <pcl/common/time.h>
#include "../include/internal.h"

boost::shared_ptr<std::vector<pcl::PointIndices>> 
growingRegionSegmentation(const pcl::PointCloud<pcl::device::float8> &distance_map,const pcl::PointCloud<pcl::device::float8> &color_d_map, const pcl::PointCloud<pcl::device::float8> &normals_angle_map,
													const pcl::PointCloud<pcl::PointXYZRGBA> &cloud, const float distance_threshold_, const float color_segmentation_threshold_ )
{
	// thresholds;
	// threshold = mean 8-neighborhood + (std factor* std)
	float color_threshold = 0.0377 + 0.1*0.0380;//color_segmentation_threshold_;
	float cos_angle_tolerance =  1.3693 + 1*4.0190;//5;
	float distance_threshold =  0.0031 + 1*0.0065; //distance_threshold_;
	
	boost::shared_ptr<int> isOcupied = boost::shared_ptr<int> (new int[cloud.height*cloud.width]);
	boost::shared_ptr<std::vector<pcl::PointIndices>> regions = boost::shared_ptr<std::vector<pcl::PointIndices>>(new std::vector<pcl::PointIndices>());
	
	for(int i=0;i<cloud.height;i++)
		for(int j=0;j<cloud.width;j++)
			isOcupied.get()[i*cloud.width+j]=0;

	int region_number = 0;
	float nan_value = std::numeric_limits<float>::quiet_NaN();

	// initial index
	int irow,icol;
	irow = icol = 0;

	pcl::PointIndices zero;
	regions->push_back(zero);

	// seeds;
	std::stack<int> seeds;

	for( int i=0; i<cloud.size();i++)
	{
		if( isOcupied.get()[i] )
			continue;

		// starting new region
		if( !pcl::isFinite(cloud.points[i]) )
		{
			isOcupied.get()[i] = 1;
			regions->data()[0].indices.push_back(i);
			continue;
		}

		// a new point for starting the region expansion
		seeds.push(i);
		region_number++;
		pcl::PointIndices new_region;
		regions->push_back(new_region);

		while(seeds.size()!=0)
		{
			int actual_index = seeds.top();
			seeds.pop();
		
			// if the point is not finite, add to the zero region
			if( cloud.points[actual_index].x == nan_value || cloud.points[actual_index].y == nan_value || cloud.points[actual_index].z == nan_value )
			{
				regions->data()[0].indices.push_back(actual_index);
				isOcupied.get()[actual_index] = 1;
			}
			// try to expand region by its inmediate neighborhod right and down
			else
			{
				// 8-neighborhood vertical-horizontal-diagonal
				int leftup_neighbor = actual_index-cloud.width-1;
				int up_neighbor = actual_index-cloud.width;
				int rightup_neighbor = actual_index-cloud.width+1;
				int left_neighbor = actual_index-1;
				int leftdown_neighbor = actual_index+cloud.width-1;
				int down_neighbor = actual_index+cloud.width;
				int rightdown_neighbor = actual_index+cloud.width+1;
				int right_neighbor = actual_index+1;

				int row = (actual_index/cloud.width);
				int col = (actual_index%cloud.width);

				// leftup_neighbor
				if( row-1 >= 0 && col-1 >= 0 && 
					isOcupied.get()[leftup_neighbor] == 0 && distance_map.points[actual_index].x != nan_value && distance_map.points[actual_index].x < distance_threshold && 
					normals_angle_map[actual_index].x < cos_angle_tolerance &&
					color_d_map.points[actual_index].x < color_threshold )
				{
					seeds.push( leftup_neighbor );
					regions->at( region_number ).indices.push_back( leftup_neighbor );
					isOcupied.get()[leftup_neighbor] = 1;
				}

				//up_neighbor
				if( row-1 >= 0 && 
					isOcupied.get()[up_neighbor] == 0 && distance_map.points[actual_index].y != nan_value && distance_map.points[actual_index].y < distance_threshold &&
					normals_angle_map[actual_index].y < cos_angle_tolerance &&
					color_d_map.points[actual_index].y < color_threshold )
				{
					seeds.push( up_neighbor );
					regions->at( region_number ).indices.push_back( up_neighbor );
					isOcupied.get()[up_neighbor] = 1;
				}

				//rightup_neighbor
				if( row-1 >= 0 && col+1 < cloud.width &&
					isOcupied.get()[rightup_neighbor] == 0 && distance_map.points[actual_index].z != nan_value && distance_map.points[actual_index].z < distance_threshold &&
					normals_angle_map[actual_index].z < cos_angle_tolerance &&
					color_d_map.points[actual_index].z < color_threshold )
				{
					seeds.push( rightup_neighbor );
					regions->at( region_number ).indices.push_back( rightup_neighbor );
					isOcupied.get()[rightup_neighbor] = 1;
				}

				// right
				if( col+1 < cloud.width  &&
					isOcupied.get()[right_neighbor] == 0 && distance_map.points[actual_index].c1 != nan_value && distance_map.points[actual_index].c1 < distance_threshold &&
					normals_angle_map[actual_index].c1 < cos_angle_tolerance &&
					color_d_map.points[actual_index].c1 < color_threshold )
				{
					seeds.push( right_neighbor );
					regions->at( region_number ).indices.push_back( right_neighbor );
					isOcupied.get()[right_neighbor] = 1;
				}

				// right down
				if( col+1 < cloud.width  && row+1 < cloud.height &&
					isOcupied.get()[rightdown_neighbor] == 0 && distance_map.points[actual_index].c4 != nan_value && distance_map.points[actual_index].c4 < distance_threshold &&
					normals_angle_map[actual_index].c4 < cos_angle_tolerance &&
					color_d_map.points[actual_index].c4 < color_threshold )
				{
					seeds.push( rightdown_neighbor );
					regions->at( region_number ).indices.push_back( rightdown_neighbor );
					isOcupied.get()[rightdown_neighbor] = 1;
				}

				// down
				if( row+1 < cloud.height && 
					isOcupied.get()[down_neighbor] == 0 && distance_map.points[actual_index].c3 != nan_value && distance_map.points[actual_index].c3 < distance_threshold &&
					normals_angle_map[actual_index].c3 < cos_angle_tolerance &&
					color_d_map.points[actual_index].c3 < color_threshold )
				{
					seeds.push( down_neighbor );
					regions->at( region_number ).indices.push_back( down_neighbor );
					isOcupied.get()[down_neighbor] = 1;
				}

				//leftdown
				if( row+1 < cloud.height && col-1 >= 0 && 
					isOcupied.get()[leftdown_neighbor] == 0 && distance_map.points[actual_index].c2 != nan_value && distance_map.points[actual_index].c2 < distance_threshold &&
					normals_angle_map[actual_index].c2 < cos_angle_tolerance &&
					color_d_map.points[actual_index].c2 < color_threshold )
				{
					seeds.push( leftdown_neighbor );
					regions->at( region_number ).indices.push_back( leftdown_neighbor );
					isOcupied.get()[leftdown_neighbor] = 1;
				}

				// left
				if( col-1 >= 0 &&
					isOcupied.get()[left_neighbor] == 0 && distance_map.points[actual_index].w != nan_value && distance_map.points[actual_index].w < distance_threshold &&
					normals_angle_map[actual_index].w < cos_angle_tolerance &&
					color_d_map.points[actual_index].w < color_threshold )
				{
					seeds.push( left_neighbor );
					regions->at( region_number ).indices.push_back( left_neighbor );
					isOcupied.get()[left_neighbor] = 1;
				}
			}
		}
	}

	/*std::vector<pcl::PointIndices>::iterator i;
	for(i=regions->begin();i!=regions->end();)
	{
		if( i->indices.size() < 300 )
			i=regions->erase(i);
		else
			i++;
	}*/
	return regions;
}

pcl::PointCloud<pcl::PointXYZRGBA>::Ptr
getColoredCloud (boost::shared_ptr<std::vector<pcl::PointIndices>> segments_, const pcl::PointCloud<pcl::PointXYZRGBA> &cloud )
{
  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr colored_cloud;

  if (!segments_->empty ())
  {
    colored_cloud = (new pcl::PointCloud<pcl::PointXYZRGBA>)->makeShared ();

    srand (static_cast<unsigned int> (time (0)));
    std::vector<unsigned char> colors;
    for (size_t i_segment = 0; i_segment < segments_->size ()-1; i_segment++)
    {
      colors.push_back (static_cast<unsigned char> (rand () % 256));
      colors.push_back (static_cast<unsigned char> (rand () % 256));
      colors.push_back (static_cast<unsigned char> (rand () % 256));
    }

    colored_cloud->width = cloud.width;
    colored_cloud->height = cloud.height;
    colored_cloud->is_dense = cloud.is_dense;
    for (size_t i_point = 0; i_point < cloud.points.size (); i_point++)
    {
      pcl::PointXYZRGBA point;
      point.x = cloud.points[i_point].x;
      point.y = cloud.points[i_point].y;
      point.z = cloud.points[i_point].z;
      colored_cloud->points.push_back (point);
    }

		std::vector<pcl::PointIndices>::iterator i_segment;
		i_segment = segments_->begin ();
		i_segment++;
    int next_color = 0;
    for (; i_segment != segments_->end (); i_segment++)
    {
      std::vector<int>::iterator i_point;
			for (i_point = i_segment->indices.begin (); i_point != i_segment->indices.end (); i_point++)
      {
        int index;
        index = *i_point;
        colored_cloud->points[index].r = colors[3 * next_color];
        colored_cloud->points[index].g = colors[3 * next_color + 1];
        colored_cloud->points[index].b = colors[3 * next_color + 2];
      }
      next_color++;
    }

		colored_cloud->is_dense = false;
		colored_cloud->sensor_origin_.setZero ();
		colored_cloud->sensor_orientation_.w () = 0.0;
		colored_cloud->sensor_orientation_.x () = 1.0;
		colored_cloud->sensor_orientation_.y () = 0.0;
		colored_cloud->sensor_orientation_.z () = 0.0;
  }

  return (colored_cloud);
}