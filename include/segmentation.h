#ifndef __SEGMENTATION__
#define __SEGMENTATION__

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/pcl_base.h>
#include <pcl/pcl_macros.h>
#include <pcl/common/time.h>
#include <boost/filesystem.hpp>

boost::shared_ptr<std::vector<pcl::PointIndices>> 
growingRegionSegmentation(const pcl::PointCloud<pcl::device::float8> &distance_map,const pcl::PointCloud<pcl::device::float8> &color_d_map, const pcl::PointCloud<pcl::device::float8> &normals_angle_map,
													const pcl::PointCloud<pcl::PointXYZRGBA> &cloud, const float distance_threshold_, const float color_segmentation_threshold_ );

pcl::PointCloud<pcl::PointXYZRGBA>::Ptr
getColoredCloud (boost::shared_ptr<std::vector<pcl::PointIndices>> segments_, const pcl::PointCloud<pcl::PointXYZRGBA> &cloud );

#endif