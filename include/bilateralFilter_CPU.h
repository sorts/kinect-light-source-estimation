#ifndef CPU_UTILS_
#define CPU_UTILS_

void
bilateralFilter_CPU(const std::vector<unsigned short> &input_depth, std::vector<unsigned short> &output_depth,const float sigma_space2_inv_half,const float sigma_color2_inv_half,const int width,const int height);

void 
computeSegmentationGraphCPU(const pcl::PointCloud<pcl::PointXYZRGB> &color_cloud, pcl::PointCloud<pcl::device::float8> &distance_map,
	pcl::PointCloud<pcl::device::float8> &color_d_map, pcl::PointCloud<pcl::device::float8> &normals_angle_map);

void
computeDistancesMapCPU (const pcl::PointCloud<pcl::PointXYZRGB> &cloud, const float3 point, pcl::PointCloud<float> &distances_map, pcl::PointCloud<float3> &normalized_directions, int rows, int cols);

void
computeAttenuationReflectanceAlbedoMapsCPU(const pcl::PointCloud<float> &dmap_pointLight,const pcl::PointCloud<float> &dmap_pointView, pcl::PointCloud<float> &attenuation_map, 
			const pcl::PointCloud<pcl::Normal> &nmap, const pcl::PointCloud<float3> &directions_pointLight, pcl::PointCloud<float> &reflectance_map, const float attenuationFactor, 
			const std::vector<float> &grayscaleImage,	pcl::PointCloud<float> &albedo_map, const int rows, const int cols);

void
convertRGBtoGrayKernel (int rows, int cols, const pcl::PointCloud<pcl::PointXYZRGB> &cmap, std::vector<float> &gray_map, const int conversion_type);

double 
computeCloudResolution (const pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr &cloud);



#endif